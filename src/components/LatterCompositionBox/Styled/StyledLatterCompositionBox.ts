import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

export interface StyledDiscographyInfoBoxProps {
  isReversed?: boolean;
  isNoPaddingBottom?: boolean;
}

export const StyledLatterCompositionBox = styled.div<StyledDiscographyInfoBoxProps>`
  display: flex;
  flex-direction: column;
  padding-bottom: ${({ isNoPaddingBottom }) => (isNoPaddingBottom ? 0 : 50)}px;
  min-height: 200px;
  ${MediaQueries.laptop} {
    flex-direction: ${({ isReversed }) => (isReversed ? 'row-reverse' : 'row')};
    min-height: 400px;
  }
`;
