import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

export const StyledTitleInfoBox = styled.h3`
  font-size: ${({ theme }) => theme.sizes.title.fontSizeMobile}px;
  line-height: ${({ theme }) => theme.sizes.title.lineHeightMobile}px;
  color: rgb(${({ theme }) => theme.lightModeColors.textPrimary});
  letter-spacing: 0;
  font-weight: 400;
  text-transform: uppercase;
  padding-bottom: 20px;

  ${MediaQueries.laptop} {
    font-size: ${({ theme }) => theme.sizes.title.fontSizeDesktop}px;
    line-height: ${({ theme }) => theme.sizes.title.lineHeightDesktop}px;
  }
`;
