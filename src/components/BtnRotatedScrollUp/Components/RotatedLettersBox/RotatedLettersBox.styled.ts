import styled from 'styled-components';

export interface StyledRotatedLettersBoxProps {
  height: number;
}

export const StyledRotatedLettersBox = styled.div<StyledRotatedLettersBoxProps>`
  height: ${({ height }) => height}px;
`;
