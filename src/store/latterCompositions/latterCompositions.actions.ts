import { COMPOSITIONS_STATES } from './latterCompositions.constants';
import {
  fetchAllLatterCompositions,
  pushNewLatterComposition,
} from './latterCompositions.fetch';
import { LatterCompositionBoxProps } from 'components/LatterCompositionBox/LatterCompositionBox';
import { ILatterCompositionForm } from 'components/AddCompositionForm/data/ILatterCompositionForm';
import { ButtonVariants } from 'components/OutsidePageLink/OutsidePageLink';
import { SocialMediaCounterIcons } from 'components/SocialMediaCounter/SocialMediaCounter';

export const fetchAllCompositions = () => async (dispatch: any) => {
  dispatch({
    type: COMPOSITIONS_STATES.COMPOSITIONS_GET_REQUEST,
  });

  try {
    const response = await fetchAllLatterCompositions();

    const data: LatterCompositionBoxProps[] = await response.json();

    dispatch({
      type: COMPOSITIONS_STATES.COMPOSITIONS_GET_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: COMPOSITIONS_STATES.COMPOSITIONS_GET_FAILURE,
    });
  }
};

export const addPendingComposition = (composition: ILatterCompositionForm) => ({
  type: COMPOSITIONS_STATES.ADD_PENDING_COMPOSITION,
  payload: composition,
});

export const modifyPendingComposition = (
  composition: ILatterCompositionForm,
) => ({
  type: COMPOSITIONS_STATES.MODIFY_PENDING_COMPOSITION,
  payload: composition,
});

export const clearPendingComposition = () => ({
  type: COMPOSITIONS_STATES.CLEAR_PENDING_COMPOSITION,
});

export const pushPendingCompositionToServer =
  (composition: ILatterCompositionForm) => async (dispatch: any) => {
    const dateFrom1970AsNumber = Date.parse(composition.date);
    const embedLink = composition.link.replace('watch?v=', 'embed/');
    const compositionToPush: LatterCompositionBoxProps = {
      BoxData: {
        vertical: true,
        InfoBoxData: {
          title: composition.title,
          description: composition.description,
          btnData: {
            label: 'Visit The iTunes',
            to: composition.link,
            background: '',
            btnVariant: ButtonVariants.SECONDARY,
          },
          socialMediaItemsData: [
            {
              likesNumber: 12994197,
              icon: SocialMediaCounterIcons.COMMENT,
            },
            {
              likesNumber: 4354,
              icon: SocialMediaCounterIcons.COMMENT,
            },
            {
              likesNumber: 345,
              icon: SocialMediaCounterIcons.COMMENT,
            },
          ],
        },
        TimelineDateData: {
          timelineBottomSpace: 30,
          dateTextData: {
            date: dateFrom1970AsNumber,
            showFullDate: true,
          },
          title: composition.dataTitle,
        },
      },
      VideoBoxData: {
        title: 'video title',
        url: embedLink,
        noFullScreen: false,
      },
    };

    try {
      const response = await pushNewLatterComposition(compositionToPush);

      const data: LatterCompositionBoxProps = await response.json();

      await dispatch({
        type: COMPOSITIONS_STATES.ADD_NEW_COMPOSITION,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: COMPOSITIONS_STATES.COMPOSITIONS_GET_FAILURE,
      });
    }

    return {
      type: COMPOSITIONS_STATES.CLEAR_PENDING_COMPOSITION,
    };
  };
