import { memo, useCallback, useEffect, useState } from 'react';
import CurvedLetter from './Components/CurvedLetter/CurvedLetter';
import { StyledBtnRotatedScrollUp } from './BtnRotatedScrollUp.styled';
import RotatedLettersBox from './Components/RotatedLettersBox/RotatedLettersBox';
import RotatedScrollUpArrow from './Components/RotatedScrollUpArrow/RotatedScrollUpArrow';
import { Breakpoints } from 'utils/MediaQueries';
import { navDesktopHeight } from 'utils/navHeight';
export interface BtnRotatedScrollUpProps {
  text: string;
  cirlceRadius: number;
}

const BtnRotatedScrollUp: React.FC<BtnRotatedScrollUpProps> = ({
  text,
  cirlceRadius,
}) => {
  const cirularText = (txt: string, radius: number = cirlceRadius) => {
    const txtLetters = txt.split('');
    const deg = 360 / (txt.length + 1);
    let origin = 0;

    const curvedLettersToDisplay = txtLetters.map(
      (letter: string, letterIndex: number) => {
        const newItem = (
          <CurvedLetter
            key={letterIndex}
            origin={origin}
            radius={radius}
            text={letter}
            isUpperCase={true}
          />
        );
        origin += deg;
        return newItem;
      },
    );
    return curvedLettersToDisplay;
  };

  const [rotateDegOnScroll, setRotateDegOnScroll] = useState(0);
  const [isRotateBtnVisible, setIsRotateBtnVisible] = useState(false);

  const rotateOnScroll = useCallback(() => {
    if (window.innerWidth < Breakpoints.laptop) return;
    const pageYOffset = window.pageYOffset;
    if (pageYOffset > window.innerHeight - navDesktopHeight) {
      if (!isRotateBtnVisible) {
        setIsRotateBtnVisible(true);
      }
    } else {
      if (isRotateBtnVisible) {
        setIsRotateBtnVisible(false);
      }
    }

    setRotateDegOnScroll(pageYOffset * 0.4);
  }, [isRotateBtnVisible]);

  useEffect(() => {
    window.addEventListener('scroll', rotateOnScroll);
  }, [rotateOnScroll]);

  return (
    <StyledBtnRotatedScrollUp
      width={2 * cirlceRadius}
      isVisible={isRotateBtnVisible}
    >
      <RotatedLettersBox
        height={2 * cirlceRadius}
        rotateDegValue={rotateDegOnScroll}
      >
        {cirularText(text, cirlceRadius)}
      </RotatedLettersBox>

      <RotatedScrollUpArrow />
    </StyledBtnRotatedScrollUp>
  );
};

export default memo(BtnRotatedScrollUp);
