import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';
export const StyledInfoBox = styled.div`
  width: 100%;
  ${MediaQueries.laptop} {
    max-width: 670px;
  }
`;
