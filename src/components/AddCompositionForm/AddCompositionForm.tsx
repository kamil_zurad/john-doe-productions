import { lazy } from 'react';
import { useState } from 'react';
import { Form, Field } from 'react-final-form';
import { useDispatch, useSelector } from 'react-redux';
import {
  addPendingComposition,
  clearPendingComposition,
  modifyPendingComposition,
} from 'store/latterCompositions/latterCompositions.actions';
import buttonBG from 'assets/images/Btns/btnBG.jpg';
import buttonBG2 from 'assets/images/Btns/btnBG2.jpg';
import { RootState } from 'store/rootReducer';
import { ILatterCompositionForm } from './data/ILatterCompositionForm';
import {
  StyledAddCompositionForm,
  StyledButtonsWrapper,
} from './AddCompositionForm.styled';
import SubmitButton from './components/SubmitButton/SubmitButton';
import Button from 'components/Button/Button';
import InputWithLabel from 'components/InputWithLabel/InputWithLabel';
const FormSummary = lazy(() => import('components/FormSummary/FormSummary'));

export interface AddCompositionFormProps {}

const required = (value: any) => (value ? undefined : 'Required');

const AddCompositionForm: React.FC<AddCompositionFormProps> = () => {
  const [isSummared, setIsSummared] = useState(false);
  const dispatch = useDispatch();
  const compositionPendingToAdd: ILatterCompositionForm = useSelector(
    (state: RootState) => state.latterCompositions.compositionPendingToBeAdded,
  );

  const goBackToForm = () => setIsSummared(false);

  const handleSubmit = (values: ILatterCompositionForm) => {
    setIsSummared(!isSummared);

    const composition: ILatterCompositionForm = {
      title: values.title,
      description: values.description,
      link: values.link,
      date: values.date,
      dataTitle: values.dataTitle,
    };

    if (compositionPendingToAdd !== null) {
      dispatch(modifyPendingComposition(composition));
    } else {
      dispatch(addPendingComposition(composition));
    }
  };

  return (
    <Form
      onSubmit={(e: ILatterCompositionForm) => handleSubmit(e)}
      render={
        !isSummared
          ? ({ handleSubmit, form, submitting, pristine, values }) => {
              const handleReset = () => {
                form.reset();
                if (compositionPendingToAdd !== null) {
                  dispatch(clearPendingComposition());
                }
              };

              return (
                <StyledAddCompositionForm onSubmit={handleSubmit}>
                  <Field name="title" validate={required}>
                    {({ input, meta }) => (
                      <InputWithLabel
                        title="title"
                        inputData={input}
                        type="text"
                        isNotValidate={meta.error && meta.touched}
                        placeholder={
                          meta.error && meta.touched ? `required` : ''
                        }
                      />
                    )}
                  </Field>
                  <Field name="description" validate={required}>
                    {({ input, meta }) => (
                      <InputWithLabel
                        title="description"
                        inputData={input}
                        type="text"
                        isNotValidate={meta.error && meta.touched}
                        placeholder={
                          meta.error && meta.touched ? `required` : ''
                        }
                      />
                    )}
                  </Field>
                  <Field name="link" validate={required}>
                    {({ input, meta }) => (
                      <InputWithLabel
                        title="YouTube link"
                        inputData={input}
                        type="text"
                        isNotValidate={meta.error && meta.touched}
                        placeholder={
                          meta.error && meta.touched ? `required` : ''
                        }
                      />
                    )}
                  </Field>
                  <Field name="dataTitle" validate={required}>
                    {({ input, meta }) => (
                      <InputWithLabel
                        title="Data title"
                        inputData={input}
                        type="text"
                        isNotValidate={meta.error && meta.touched}
                        placeholder={
                          meta.error && meta.touched ? `required` : ''
                        }
                      />
                    )}
                  </Field>
                  <Field name="date" validate={required}>
                    {({ input, meta }) => (
                      <InputWithLabel
                        title="date"
                        inputData={input}
                        type="date"
                        isNotValidate={meta.error && meta.touched}
                        placeholder={
                          meta.error && meta.touched ? `required` : ''
                        }
                      />
                    )}
                  </Field>
                  <StyledButtonsWrapper className="buttons">
                    <Button
                      onClickFunction={handleReset}
                      disabled={submitting || pristine}
                      background={buttonBG}
                      marginRight={10}
                    >
                      Reset
                    </Button>

                    <SubmitButton disabled={submitting} background={buttonBG2}>
                      {`Summary`}
                    </SubmitButton>
                  </StyledButtonsWrapper>
                </StyledAddCompositionForm>
              );
            }
          : () => <FormSummary backToForm={goBackToForm} />
      }
    />
  );
};

export default AddCompositionForm;
