import {
  StyledInfoBox,
  StyledInfoBoxBtnWrapper,
  StyledControlsWrapper,
} from './InfoBox.styled';
import TitleInfoBox from 'components/TitleInfoBox/TitleInfoBox';
import DescriptionInfo from 'components/DescriptionInfo/DescriptionInfo';
import Button, {
  ButtonProps,
} from 'components/OutsidePageLink/OutsidePageLink';
import SocialMediaCounters from 'components/SocialMediaCounters/SocialMediaCounters';
import { SocialMediaCounterProps } from 'components/SocialMediaCounter/SocialMediaCounter';

export interface InfoBoxProps {
  title: string;
  description: string;
  btnData: ButtonProps;
  socialMediaItemsData?: SocialMediaCounterProps[];
}

const InfoBox: React.FC<InfoBoxProps> = ({
  title,
  description,
  btnData,
  socialMediaItemsData,
}) => {
  return (
    <StyledInfoBox>
      <TitleInfoBox>{title}</TitleInfoBox>
      <DescriptionInfo>{description}</DescriptionInfo>
      <StyledInfoBoxBtnWrapper>
        <Button {...btnData} />
        {socialMediaItemsData && (
          <StyledControlsWrapper>
            <SocialMediaCounters socialMediaItemsData={socialMediaItemsData} />
          </StyledControlsWrapper>
        )}
      </StyledInfoBoxBtnWrapper>
    </StyledInfoBox>
  );
};

export default InfoBox;
