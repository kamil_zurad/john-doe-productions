import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';
import { navMobileHeight } from 'utils/navHeight';
import headerBG from 'assets/images/header/headerBG.png';

export const StyledHeader = styled.header`
  width: 100%;
  min-height: calc(100vh - ${navMobileHeight}px);
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  background-image: url(${headerBG});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;

  ${MediaQueries.laptop} {
    min-height: 100vh;
  }
`;
