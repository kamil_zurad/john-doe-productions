import styled from 'styled-components';
import { Link } from 'react-scroll';
import MediaQueries from 'utils/MediaQueries';
export interface StyledScrollingLinkProps {
  $isLinkFloorVisible: boolean;
  $isCurrentlyTracked: boolean;
}

export const StyledScrollingLink = styled(Link)<StyledScrollingLinkProps>`
  text-transform: uppercase;
  align-items: center;
  display: flex;
  height: auto;
  color: rgb(${({ theme }) => theme.lightModeColors.textPrimary});
  position: relative;
  overflow: hidden;
  line-height: ${({ theme }) => theme.navBar.lineHeight}px;
  letter-spacing: ${({ theme }) => theme.navBar.letterSpacing}px;
  font-size: ${({ theme }) => theme.navBar.fontSize}px;
  text-decoration: none;
  font-family: 'Dosis';

  &::before {
    content: '';
    position: absolute;
    left: 0;
    bottom: 0;
    width: 100%;
    height: 3px;
    border-radius: 5px;
    background-color: rgb(${({ theme }) => theme.lightModeColors.textPrimary});
    transition: transform 0.3s cubic-bezier(0.39, 0.99, 0.18, 1.35);
    transform: translateX(-100%);

    ${MediaQueries.laptop} {
      transform: translateX(
        ${({ $isLinkFloorVisible, $isCurrentlyTracked }) =>
          $isCurrentlyTracked ? '0' : $isLinkFloorVisible ? '0' : '-100%'}
      );
    }
  }
`;
