import { StyledPageWrapper } from 'components/StyledPageWrapper/StyledPageWrapper';
import SectionHeader from 'components/SectionHeader/SectionHeader';
import SectionDiscography from 'components/SectionDiscography/SectionDiscography';
import SectionConcertTours from 'components/SectionConcertTours/SectionConcertTours';
import SectionLatterCompositions from 'components/SectionLatterCompositions/SectionLatterCompositions';
import Navigation from 'components/Navigation/Navigation';
import BtnRotatedScrollUp from 'components/BtnRotatedScrollUp/BtnRotatedScrollUp';
export interface HomepageProps {}

export const Homepage: React.FC<HomepageProps> = () => {
  return (
    <>
      <Navigation />
      <StyledPageWrapper>
        <SectionHeader />
        <SectionDiscography />
        <SectionConcertTours />
        <SectionLatterCompositions />
      </StyledPageWrapper>
      <BtnRotatedScrollUp text="John Doe Music Start Now" cirlceRadius={100} />
    </>
  );
};
