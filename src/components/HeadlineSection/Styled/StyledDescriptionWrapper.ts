import styled from 'styled-components';

export const StyledDescriptionWrapper = styled.div`
  max-width: 735px;
  margin-top: ${({ theme }) => theme.sizes.sectionTitle.fontSizeMobile}px;
`;
