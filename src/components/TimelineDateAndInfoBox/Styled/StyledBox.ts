import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

export interface StyledBoxProps {
  vertical?: boolean;
}

export const StyledBox = styled.div<StyledBoxProps>`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: 8px;

  ${MediaQueries.laptop} {
    align-items: normal;
    flex-direction: ${({ vertical }) => (vertical ? 'column' : 'row')};
    padding: 0px;
  }
`;
