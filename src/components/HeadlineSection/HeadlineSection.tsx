import { memo } from 'react';
import {
  StyledHeadlineSection,
  StyledDescriptionWrapper,
} from './HeadlineSection.styled';
import TitleSection from 'components/TitleSection/TitleSection';
import DescriptionSection from 'components/DescriptionSection/DescriptionSection';

export interface HeadlineSectionProps {
  titleText: string;
  titleFirstColor?: string;
  titleSecondColor?: string;
  noTitleLineThrought?: boolean;
  descriptionText: string;
}

const HeadlineSection: React.FC<HeadlineSectionProps> = ({
  titleText,
  titleFirstColor,
  titleSecondColor,
  noTitleLineThrought,
  descriptionText,
}) => {
  return (
    <StyledHeadlineSection>
      <TitleSection
        text={titleText}
        firstColor={titleFirstColor}
        secondColor={titleSecondColor}
        noLineThrought={noTitleLineThrought}
      />
      <StyledDescriptionWrapper>
        <DescriptionSection text={descriptionText} />
      </StyledDescriptionWrapper>
    </StyledHeadlineSection>
  );
};

export default memo(HeadlineSection);
