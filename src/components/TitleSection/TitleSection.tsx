import {
  StyledTitleSection,
  StyledTitleSectionWrapper,
} from './TitleSection.styled';

export interface SectionTitleProps {
  text: string;
  firstColor?: string;
  secondColor?: string;
  noLineThrought?: boolean;
}

const TitleSection: React.FC<SectionTitleProps> = ({
  text,
  firstColor,
  secondColor,
  noLineThrought,
}) => {
  return (
    <StyledTitleSectionWrapper noLineThrought={noLineThrought}>
      <StyledTitleSection firstColor={firstColor} secondColor={secondColor}>
        {text}
      </StyledTitleSection>
    </StyledTitleSectionWrapper>
  );
};

export default TitleSection;
