export default class ViewEventConditionals {
  public static isSectionVisibleToTrackNavItem(
    sectionToScroll: HTMLElement,
    navHeight: number,
  ) {
    return !!(
      window.pageYOffset >= sectionToScroll.offsetTop - 1 * navHeight &&
      window.pageYOffset <=
        sectionToScroll.offsetTop + sectionToScroll.clientHeight - 1 * navHeight
    );
  }

  public static isSectionVisible(sectionToScroll: HTMLElement) {
    return !!(
      window.pageYOffset >= sectionToScroll.offsetTop &&
      window.pageYOffset <=
        sectionToScroll.offsetTop + sectionToScroll.clientHeight
    );
  }

  public static isResizedButStillBeneathThreshold(
    referenceScreenWidth: number,
    threshold: number,
  ) {
    return !!(
      referenceScreenWidth < threshold && window.innerWidth < threshold
    );
  }

  public static isResizedButStillAboveThreshold(
    referenceScreenWidth: number,
    threshold: number,
  ) {
    return !!(
      referenceScreenWidth >= threshold && window.innerWidth >= threshold
    );
  }

  public static isResizedFromMobileToDesktop(
    referenceScreenWidth: number,
    threshold: number,
  ) {
    return !!(
      referenceScreenWidth < threshold && window.innerWidth >= threshold
    );
  }
}
