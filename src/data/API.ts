const serverUrl = 'https://mobitouch-json-server.herokuapp.com'; // use for real server
// const localUrl = 'http://localhost:3001'; // use if you want to run JSON server locally on your
export class API {
  public static url = serverUrl;
}
