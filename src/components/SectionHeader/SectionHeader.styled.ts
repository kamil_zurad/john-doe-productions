export { StyledHeader } from './Styled/StyledHeader';
export { StyledHeaderShowcaseWrapper } from './Styled/StyledHeaderShowcaseWrapper';
export { StyledScrollDownBtnWrapper } from './Styled/StyledScrollDownBtnWrapper';
