import { API } from 'data/API';
import { LatterCompositionBoxProps } from 'components/LatterCompositionBox/LatterCompositionBox';
import { initialState } from './latterCompositions.reducer';
export const fetchAllLatterCompositions = () => {
  const promise = fetch(`${API.url}/latterCompositions`);

  return promise;
};

export interface ILatterCompositionToPush extends LatterCompositionBoxProps {
  id: number;
}

export const pushNewLatterComposition = (data: LatterCompositionBoxProps) => {
  const compositionToPush: ILatterCompositionToPush = {
    id: initialState.compositions.length,
    ...data,
  };

  const promise = fetch(`${API.url}/latterCompositions`, {
    method: 'POST',
    headers: {
      'Content-type': 'application/json',
    },
    body: JSON.stringify(compositionToPush),
  });

  return promise;
};
