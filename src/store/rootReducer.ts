import { combineReducers } from 'redux';

import mobileNavScroll from './mobileNavScroll/mobileNavScroll.reducer';
import latterCompositions from './latterCompositions/latterCompositions.reducer';

const rootReducer = combineReducers({
  mobileNavScroll,
  latterCompositions,
});

export default rootReducer;

export type RootState = ReturnType<typeof rootReducer>;
