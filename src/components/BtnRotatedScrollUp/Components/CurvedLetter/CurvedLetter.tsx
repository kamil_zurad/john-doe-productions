import { memo } from 'react';
import { StyledCurvedLetter } from './CurvedLetter.styled';

export interface CurvedLetterProps {
  radius: number;
  origin: number;
  text: string;
  isUpperCase?: boolean;
}

const CurvedLetter: React.FC<CurvedLetterProps> = ({
  radius,
  origin,
  text,
  isUpperCase,
}) => {
  return (
    <StyledCurvedLetter
      $radius={radius}
      $origin={origin}
      isUpperCase={isUpperCase}
    >
      {text}
    </StyledCurvedLetter>
  );
};

export default memo(CurvedLetter);
