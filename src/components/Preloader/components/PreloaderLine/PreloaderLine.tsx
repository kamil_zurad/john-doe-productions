import { StyledPreloaderLine } from './PreloaderLine.styled';

export interface PreloaderLineProps {
  distanceToLeftSide: number;
}

const PreloaderLine: React.FC<PreloaderLineProps> = ({
  distanceToLeftSide,
}) => {
  return <StyledPreloaderLine distanceToLeftSide={distanceToLeftSide} />;
};

export default PreloaderLine;
