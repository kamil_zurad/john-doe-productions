import { StyledDescriptionInfo } from './DescriptionInfo.styled';
export interface DescriptionInfoProps {}

const DescriptionInfo: React.FC<DescriptionInfoProps> = ({ children }) => {
  return <StyledDescriptionInfo>{children}</StyledDescriptionInfo>;
};

export default DescriptionInfo;
