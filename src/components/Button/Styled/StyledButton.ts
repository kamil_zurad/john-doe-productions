import styled, { css } from 'styled-components';

export interface StyledButtonProps {
  marginRight?: number;
  disabled?: boolean;
  background?: any;
}

export const ButtonStyles = css<StyledButtonProps>`
  display: inline-block;
  min-width: 150px;
  padding: 13px 30px;
  margin-right: ${({ marginRight }) => (marginRight ? marginRight : 0)}px;
  text-transform: inherit;
  font-size: ${({ theme }) => theme.sizes.btn.fontSize}px;
  &::first-letter {
    text-transform: uppercase;
  }
  text-decoration: none;
  font-weight: bold;
  text-align: center;
  color: ${({ disabled, theme }) =>
    disabled
      ? `rgba(0,0,0,0.6)`
      : `rgb(${theme.lightModeColors.textPrimary})})`};
  opacity: ${({ disabled }) => (disabled ? '0.6' : '1')};
  background: url(${({ background }) => (background ? background : null)});
  background-position: center;
  background-size: cover;
  border: 1px solid rgba(0, 0, 0, 0.35);
  cursor: pointer;
`;

export const StyledButton = styled.button<StyledButtonProps>`
  ${ButtonStyles}
`;
