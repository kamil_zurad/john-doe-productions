import { mobileNavScrollTypes } from './mobileNavScroll.constants';

export interface mobileNavScrollActionType {
  type: mobileNavScrollTypes;
}

export const showMobileNavSmoothly = (): mobileNavScrollActionType => ({
  type: mobileNavScrollTypes.MOBILE_NAV_SHOWS_SMOOTHLY,
});

export const showMobileNavInstantly = (): mobileNavScrollActionType => ({
  type: mobileNavScrollTypes.MOBILE_NAV_SHOWS_INSTANTLY,
});

export const mobileNavIsVisible = (): mobileNavScrollActionType => ({
  type: mobileNavScrollTypes.MOBILE_NAV_IS_VISIBLE,
});

export const mobileNavIsNotVisible = (): mobileNavScrollActionType => ({
  type: mobileNavScrollTypes.MOBILE_NAV_IS_NOT_VISIBLE,
});
