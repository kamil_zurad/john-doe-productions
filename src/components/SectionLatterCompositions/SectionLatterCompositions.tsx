import { lazy, Suspense } from 'react';
import {
  StyledLatterCompositionsWrapper,
  StyledLatterCompositionsContent,
  StyledAddCompositionBtnWrapper,
} from './SectionLatterCompositions.styled';
import { StyledPageContentWrapper } from 'components/StyledPageContentWrapper/StyledPageContentWrapper';
import HeadlineSection from 'components/HeadlineSection/HeadlineSection';
import LatterCompositionBox, {
  LatterCompositionBoxProps,
} from 'components/LatterCompositionBox/LatterCompositionBox';
import { useCallback, useEffect, useMemo } from 'react';
import { fetchAllCompositions } from 'store/latterCompositions/latterCompositions.actions';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'store/rootReducer';
import PageLink from 'components/PageLink/PageLink';
import { Route, Switch } from 'react-router-dom';
import Modal from 'components/Modal/Modal';
import { Breakpoints } from 'utils/MediaQueries';
import { ButtonVariants } from 'components/OutsidePageLink/OutsidePageLink';
const AddCompositionForm = lazy(
  () => import('components/AddCompositionForm/AddCompositionForm'),
);
export interface SectionLatterCompositionsProps {}

const SectionLatterCompositions: React.FC<SectionLatterCompositionsProps> =
  () => {
    const dispatch = useDispatch();

    const latterBoxesData: LatterCompositionBoxProps[] = useSelector(
      (state: RootState) => state.latterCompositions.compositions,
    );

    useEffect(() => {
      dispatch(fetchAllCompositions());
    }, [dispatch]);

    const LatterBoxes = useMemo(
      () =>
        latterBoxesData?.map(
          (item: LatterCompositionBoxProps, itemIndex: number) => (
            <LatterCompositionBox
              index={itemIndex}
              BoxData={item.BoxData}
              VideoBoxData={item.VideoBoxData}
              key={item.BoxData.InfoBoxData.title}
              isNoPaddingBottom={++itemIndex === latterBoxesData.length}
            />
          ),
        ),
      [latterBoxesData],
    );
    const handlePageLinkClick = useCallback(() => {
      if (window.innerWidth < Breakpoints.laptop) {
        document.body.classList.add('disableScroll');
      }
    }, []);

    const LaterCompositionsContent = useMemo(
      () => (
        <StyledLatterCompositionsContent>
          {LatterBoxes !== null && LatterBoxes}
        </StyledLatterCompositionsContent>
      ),
      [LatterBoxes],
    );

    const LaterCompositionsBtn = useMemo(
      () => (
        <>
          <StyledAddCompositionBtnWrapper>
            <PageLink
              to="/addComposition"
              label="Add Composition"
              onClickFunction={handlePageLinkClick}
              btnVariant={ButtonVariants.SECONDARY}
            />
          </StyledAddCompositionBtnWrapper>
        </>
      ),
      [handlePageLinkClick],
    );

    return (
      <StyledLatterCompositionsWrapper id="LatterCompositions">
        <StyledPageContentWrapper>
          <HeadlineSection
            titleText="latter compositions"
            descriptionText={`"It's Time" was released as the lead single from Continued Silence and It's Time, both 
          extended plays preceding Night Visions' release.`}
          />
          {LaterCompositionsContent}
          {LaterCompositionsBtn}
          <Switch>
            <Route path="/addComposition">
              <Modal>
                <Suspense fallback="loading...">
                  <AddCompositionForm />
                </Suspense>
              </Modal>
            </Route>
          </Switch>
        </StyledPageContentWrapper>
      </StyledLatterCompositionsWrapper>
    );
  };

export default SectionLatterCompositions;
