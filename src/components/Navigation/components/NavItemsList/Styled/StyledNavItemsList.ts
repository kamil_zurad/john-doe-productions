import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

export interface ItemsListProps {}

export const StyledNavItemsList = styled.ul<ItemsListProps>`
  position: static;
  display: flex;
  flex-direction: column;
  list-style-type: none;
  height: auto;
  width: 100%;
  align-items: center;

  ${MediaQueries.laptop} {
    position: static;
    flex-direction: row;
    list-style-type: none;
    justify-content: space-between;
  }
`;
