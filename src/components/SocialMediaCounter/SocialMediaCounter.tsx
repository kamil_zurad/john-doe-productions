import {
  StyledSocialMediaCounter,
  StyledSocialMediaLikeText,
} from './SocialMediaCounter.styled';
import commentPNG from 'assets/images/SocialMediaIcons/comment.png';
import heartPNG from 'assets/images/SocialMediaIcons/heart.png';
import sharePNG from 'assets/images/SocialMediaIcons/share.png';
import speakerPNG from 'assets/images/SocialMediaIcons/speaker.png';

export enum SocialMediaCounterIcons {
  SPEAKER = 'SPEAKER',
  HEART = 'HEART',
  SHARE = 'SHARE',
  COMMENT = 'COMMENT',
}

export interface SocialMediaCounterProps {
  likesNumber: number;
  icon: SocialMediaCounterIcons;
}

const SocialMediaCounter: React.FC<SocialMediaCounterProps> = ({
  likesNumber,
  icon,
}) => {
  const getSocialMediaIcon = (icon: SocialMediaCounterIcons): string => {
    switch (icon) {
      case SocialMediaCounterIcons.COMMENT:
        return commentPNG;
      case SocialMediaCounterIcons.HEART:
        return heartPNG;
      case SocialMediaCounterIcons.SHARE:
        return sharePNG;
      case SocialMediaCounterIcons.SPEAKER:
        return speakerPNG;
      default:
        return speakerPNG;
    }
  };

  const getLikesNumber = (likes: number): string => {
    let likesCounter;
    let likesSymbol;
    if (likes > 999 && likes < 1000000) {
      likesSymbol = 'K';
      likesCounter = `${likes}`.slice(0, `${likes}`.length - 3);
    } else if (likes > 1000000) {
      likesSymbol = 'M';
      likesCounter = `${likes}`.slice(0, `${likes}`.length - 6);
    } else {
      likesSymbol = '';
      likesCounter = `${likes}`;
    }
    return `${likesCounter}${likesSymbol}`;
  };

  return (
    <StyledSocialMediaCounter>
      <img src={getSocialMediaIcon(icon)} alt="social media icon" />
      <StyledSocialMediaLikeText>
        {getLikesNumber(likesNumber)}
      </StyledSocialMediaLikeText>
    </StyledSocialMediaCounter>
  );
};

export default SocialMediaCounter;
