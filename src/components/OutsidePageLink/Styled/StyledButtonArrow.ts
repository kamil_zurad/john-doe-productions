import styled from 'styled-components';

export const StyledButtonArrow = styled.span`
  padding-left: 10px;
  margin-right: 5px;
`;
