import styled, { keyframes } from 'styled-components';
import { FadeOut } from './StyledPreloaderProgressBar';

const fadeInPreloaderBackground = keyframes`
  100% {
    width: 100%;
  }
`;

export const StyledProgressBarTitle = styled.span`
  position: relative;
  font-size: 40px;
  letter-spacing: 5px;
  color: black;
  font-weight: bold;
  text-transform: uppercase;
  -webkit-text-stroke: 1px rgba(255, 255, 255, 0.5);
  animation: ${FadeOut} 1s 4.5s linear forwards;

  &::before {
    content: attr(data-loading-text);
    position: absolute;
    top: 0;
    left: 0;
    width: 0;
    height: 100%;
    letter-spacing: 5px;
    color: rgba(255, 255, 255, 0.9);
    -webkit-text-stroke: 0px rgba(255, 255, 255, 0.5);
    overflow: hidden;
    animation: ${fadeInPreloaderBackground} 3.5s 0.5s steps(8, jump-end)
        forwards,
      ${FadeOut} 1s 4.5s linear forwards;
  }
`;
