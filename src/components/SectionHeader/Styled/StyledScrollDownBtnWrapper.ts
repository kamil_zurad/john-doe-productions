import styled from 'styled-components';

export const StyledScrollDownBtnWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  height: 70px;
`;
