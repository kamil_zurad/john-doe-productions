import {
  COMPOSITIONS_STATES,
  LOADING_STATES,
} from 'store/latterCompositions/latterCompositions.constants';
import { LatterCompositionBoxProps } from 'components/LatterCompositionBox/LatterCompositionBox';
import { ILatterCompositionForm } from 'components/AddCompositionForm/data/ILatterCompositionForm';
interface IinitialState {
  loadingState: LOADING_STATES;
  compositions: LatterCompositionBoxProps[];
  compositionPendingToBeAdded: ILatterCompositionForm | null;
}

export const initialState: IinitialState = {
  loadingState: LOADING_STATES.INITIAL,
  compositions: [],
  compositionPendingToBeAdded: null,
};

function latterCompositions(state = initialState, action: any) {
  switch (action.type) {
    case COMPOSITIONS_STATES.COMPOSITIONS_GET_REQUEST:
      return {
        ...state,
        loadingState: LOADING_STATES.LOADING,
      };

    case COMPOSITIONS_STATES.COMPOSITIONS_GET_SUCCESS:
      return {
        ...state,
        compositions: action.payload,
        loadingState: LOADING_STATES.LOADED,
      };

    case COMPOSITIONS_STATES.COMPOSITIONS_GET_FAILURE:
      return {
        ...state,
        loadingState: LOADING_STATES.FAILED,
      };

    case COMPOSITIONS_STATES.ADD_PENDING_COMPOSITION:
      return {
        ...state,
        compositionPendingToBeAdded: action.payload,
      };

    case COMPOSITIONS_STATES.MODIFY_PENDING_COMPOSITION:
      return {
        ...state,
        compositionPendingToBeAdded: action.payload,
      };

    case COMPOSITIONS_STATES.CLEAR_PENDING_COMPOSITION:
      return {
        ...state,
        compositionPendingToBeAdded: null,
      };

    case COMPOSITIONS_STATES.ADD_NEW_COMPOSITION:
      const newArray = [...state.compositions];
      const newComposition = action.payload;
      newArray.push(newComposition);
      return { ...state, compositions: newArray };

    default:
      return state;
  }
}

export default latterCompositions;
