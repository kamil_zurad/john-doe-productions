import styled from 'styled-components';

export const StyledArrow = styled.div`
  position: absolute;
  width: 2px;
  height: 100%;
  background-color: rgba(255, 255, 255, 0.4);
  top: 0;
  left: 50%;

  &::before,
  &::after {
    content: '';
    position: absolute;
    bottom: 0;
    left: 50%;
    height: 30%;
    width: 100%;
    background-color: rgba(255, 255, 255, 0.4);
    transform-origin: bottom;
  }

  &::before {
    transform: translateX(-50%) rotate(45deg);
  }

  &::after {
    transform: translateX(-50%) rotate(-45deg);
  }
`;
