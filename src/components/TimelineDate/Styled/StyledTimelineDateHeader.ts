import styled from 'styled-components';

export const StyledTimelineDateHeader = styled.p`
  padding-bottom: 5px;
  border-bottom: 1px solid black;
`;
