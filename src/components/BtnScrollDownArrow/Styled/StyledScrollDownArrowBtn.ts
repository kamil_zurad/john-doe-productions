import styled, { keyframes } from 'styled-components';

const shakeBtn = keyframes`
  0%{
    transform: translateY(0%);
  }
  60%{
    transform: translateY(0%);
  }
  65%{
    transform: translateY(-20%);
  }
  70%{
    transform: translateY(20%);
  }
  75%{
    transform: translateY(-20%);
  }
  80%{
    transform: translateY(20%);
  }
  85%{
    transform: translateY(-20%);
  }
  90%{
    transform: translateY(0%);
  }
  100%{
    transform: translateY(0%);
  }
`;

export const StyledScrollDownArrowBtn = styled.div`
  position: relative;
  width: 30px;
  height: 50px;
  cursor: pointer;
  animation: ${shakeBtn} 3s linear infinite;
  -webkit-tap-highlight-color: transparent;
  &:hover {
    animation: none;
  }
`;
