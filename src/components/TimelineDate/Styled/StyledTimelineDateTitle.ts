import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

export const StyledTimelineDateTitle = styled.p`
  font-size: ${({ theme }) => theme.sizes.description.fontSizeMobile}px;
  line-height: ${({ theme }) => theme.sizes.description.lineHeightMobile}px;
  color: rgb(${({ theme }) => theme.lightModeColors.textPrimary});
  font-weight: 700;
  text-transform: uppercase;

  ${MediaQueries.laptop} {
    font-size: ${({ theme }) => theme.sizes.description.fontSizeDesktop}px;
    line-height: ${({ theme }) => theme.sizes.description.lineHeightDesktop}px;
  }
`;
