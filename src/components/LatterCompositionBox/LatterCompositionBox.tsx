import {
  StyledLatterCompositionBox,
  StyledInfoBoxWrapper,
} from './LatterCompositionBox.styled';
import Box, {
  BoxProps,
} from 'components/TimelineDateAndInfoBox/TimelineDateAndInfoBox';
import VideoBox, { VideoBoxProps } from 'components/VideoBox/VideoBox';

export interface LatterCompositionBoxProps {
  id?: any;
  index?: number;
  isNoPaddingBottom?: boolean;
  BoxData: BoxProps;
  VideoBoxData: VideoBoxProps;
}

const LatterCompositionBox: React.FC<LatterCompositionBoxProps> = ({
  index,
  isNoPaddingBottom,
  BoxData,
  VideoBoxData,
}) => {
  const isReversed = ++index! % 2 === 0;
  return (
    <StyledLatterCompositionBox
      isReversed={isReversed}
      isNoPaddingBottom={isNoPaddingBottom}
    >
      <StyledInfoBoxWrapper isReversed={!isReversed}>
        <Box {...BoxData} />
      </StyledInfoBoxWrapper>
      <VideoBox {...VideoBoxData} />
    </StyledLatterCompositionBox>
  );
};

export default LatterCompositionBox;
