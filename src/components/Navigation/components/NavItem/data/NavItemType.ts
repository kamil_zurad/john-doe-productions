export interface NavItemType {
  label: string;
  sectionToScroll?: string;
}
