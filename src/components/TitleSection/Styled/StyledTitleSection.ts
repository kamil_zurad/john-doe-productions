import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

export interface StyledTitleSectionProps {
  firstColor?: string;
  secondColor?: string;
}

export const StyledTitleSection = styled.span<StyledTitleSectionProps>`
  position: relative;
  font-size: ${({ theme }) => theme.sizes.sectionTitle.fontSizeMobile}px;
  letter-spacing: 6px;
  line-height: ${({ theme }) => theme.sizes.sectionTitle.lineHeightMobile}px;
  font-family: 'Dosis';
  text-align: center;
  text-transform: uppercase;
  background: linear-gradient(
    to bottom,
    rgb(${({ firstColor }) => (firstColor ? firstColor : '0,0,0')}),
    rgb(${({ secondColor }) => (secondColor ? secondColor : '0,0,0')})
  );
  background-clip: text;
  -webkit-background-clip: text;
  color: transparent;

  ${MediaQueries.tablet} {
    font-size: ${({ theme }) => theme.sizes.sectionTitle.fontSizeDesktop}px;
    line-height: ${({ theme }) => theme.sizes.sectionTitle.lineHeightDesktop}px;
  }
`;
