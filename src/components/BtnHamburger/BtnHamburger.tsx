import { memo } from 'react';
import { StyledHamburgerBtn } from './Styled/StyledHamburgerBtn';
import {
  StyledHamburgerTopBar,
  StyledHamburgerMidBar,
  StyledHamburgerBotBar,
} from './Styled/StyledHamburgerBar';

import { useDispatch, useSelector } from 'react-redux';
import { showMobileNavSmoothly } from 'store/mobileNavScroll/mobileNavScroll.actions';
import { RootState } from 'store/rootReducer';
import {
  mobileNavIsNotVisible,
  mobileNavIsVisible,
} from 'store/mobileNavScroll/mobileNavScroll.actions';
export interface HamburgerBtnProps {}

const BtnHamburger: React.FC<HamburgerBtnProps> = () => {
  const dispatch = useDispatch();
  const isMobileNavVisible = useSelector(
    (state: RootState) => state.mobileNavScroll.isMobileNavVisible,
  );

  const toggleBodyScroll = () => {
    if (document.body.classList.contains('disableScroll')) {
      document.body.classList.remove('disableScroll');
    } else {
      document.body.classList.add('disableScroll');
    }
  };

  const toggleMobileNav = () => {
    if (isMobileNavVisible) {
      dispatch(mobileNavIsNotVisible());
    } else {
      dispatch(mobileNavIsVisible());
    }
  };

  const handleOnClick = () => {
    toggleBodyScroll();
    toggleMobileNav();
    dispatch(showMobileNavSmoothly());
  };
  return (
    <StyledHamburgerBtn onClick={handleOnClick}>
      <StyledHamburgerTopBar isBtnClicked={isMobileNavVisible} />
      <StyledHamburgerMidBar isBtnClicked={isMobileNavVisible} />
      <StyledHamburgerBotBar isBtnClicked={isMobileNavVisible} />
    </StyledHamburgerBtn>
  );
};

export default memo(BtnHamburger);
