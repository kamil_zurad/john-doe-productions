import {
  StyledInput,
  StyledInputTitle,
  StyledInputWarningWrapper,
  StyledInputWrapper,
} from './InputWithLabel.styled';
export interface InputWithLabelProps {
  inputData: any;
  isNotValidate?: boolean;
  placeholder?: string;
  type: string;
  title: string;
}

const InputWithLabel: React.FC<InputWithLabelProps> = ({
  inputData,
  isNotValidate,
  placeholder,
  type,
  title,
}) => {
  return (
    <StyledInputWrapper>
      <StyledInputTitle>{title}</StyledInputTitle>
      <StyledInputWarningWrapper>
        <StyledInput
          {...inputData}
          type={type}
          isNotValidate={isNotValidate}
          placeholder={placeholder}
        />
      </StyledInputWarningWrapper>
    </StyledInputWrapper>
  );
};

export default InputWithLabel;
