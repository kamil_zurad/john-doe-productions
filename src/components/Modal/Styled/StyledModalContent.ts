import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';
export const StyledModalContent = styled.aside`
  position: absolute;
  margin: auto;
  width: 100%;
  height: 100%;
  text-align: left;
  box-shadow: 0 0px 5px 0px rgb(0, 0, 0);
  ${MediaQueries.laptop} {
    min-height: 400px;
    min-width: 650px;
    width: auto;
    height: auto;
  }
`;
