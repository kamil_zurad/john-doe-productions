import styled from 'styled-components';

export const StyledHeaderShowcaseWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;
