import styled, { css } from 'styled-components';
import { ButtonVariants } from '../OutsidePageLink';

export interface StyledButtonProps {
  background?: any;
  variant?: ButtonVariants;
}

export const StyledButtonStyles = css<StyledButtonProps>`
  display: inline-block;
  min-width: 150px;
  padding: 13px 30px;
  text-transform: inherit;
  font-size: ${({ theme }) => theme.sizes.btn.fontSize}px;
  &::first-letter {
    text-transform: uppercase;
  }
  text-decoration: none;
  font-weight: bold;
  text-align: center;
  color: rgb(${({ theme }) => theme.lightModeColors.textPrimary});
  background: url(${({ background }) => (background ? background : null)});
  background-position: center;
  background-size: cover;
  border: ${({ background }) =>
    background ? 'none' : '1px solid rgba(0,0,0,0.35)'};
  opacity: 1;
  transition: font-size 0.3s linear;
`;

export const StyledButton = styled.a<StyledButtonProps>`
  ${StyledButtonStyles}
`;
