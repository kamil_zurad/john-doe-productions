import styled, { keyframes, css } from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

const showNav = keyframes`
  100% {
    top: 0%;
  }
`;

export interface StyledNavProps {
  isSticky: boolean;
}

export const StyledNav = styled.nav<StyledNavProps>`
  background-color: rgba(229, 230, 230, 1);
  height: ${({ theme }) => theme.navBar.mobileHeight}px;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  animation: none;
  z-index: 10;
  box-shadow: 0 0px 3px 0px
    rgb(${({ theme }) => theme.lightModeColors.textPrimary});

  ${MediaQueries.laptop} {
    box-shadow: none;
    background-color: rgba(
      ${({ theme, isSticky }) =>
        isSticky
          ? `${theme.lightModeColors.navBorderColor},0.8`
          : `${theme.navBar.navBg},0.1`}
    );
    border-bottom: rgb(
      ${({ isSticky, theme }) =>
        isSticky ? `1px solid ${theme.lightModeColors.navBorderColor}` : 'none'}
    );
    height: ${({ theme }) => theme.navBar.desktopHeight}px;
    position: ${({ isSticky }) => (isSticky ? 'fixed' : 'absolute')};
    top: ${({ isSticky }) => (isSticky ? -100 : 0)}%;
    animation: ${({ isSticky }) =>
      isSticky
        ? css`
            ${showNav} 1.5s 0s linear forwards
          `
        : 'none'};
  }
`;
