import { StyledButton, StyledButtonArrow } from './OutsidePageLink.styled';

export enum ButtonVariants {
  PRIMARY = 'PRIMARY',
  SECONDARY = 'SECONDARY',
}

export interface ButtonProps {
  to: string;
  label: string;
  btnVariant?: ButtonVariants;
  background?: React.ReactChild;
}

const Button: React.FC<ButtonProps> = ({
  to,
  label,
  background,
  btnVariant,
}) => {
  const btnArrow = btnVariant === ButtonVariants.SECONDARY && (
    <StyledButtonArrow>{`>`}</StyledButtonArrow>
  );

  return (
    <StyledButton href={to} background={background} target="_blank">
      {label}
      {btnArrow}
    </StyledButton>
  );
};

export default Button;
