import styled, { keyframes } from 'styled-components';
import MediaQueries from 'utils/MediaQueries';
import { LogoStyles } from 'components/SectionHeader/components/HeaderShowcase/Styled/StyledHeaderImage';

const liftLogoUpAndHide = keyframes`
  100%{ 
    opacity: 0;
    transform: translateY(-20%);
  }
`;

export const StyledPreloaderLogo = styled.img`
  display: block;
  ${LogoStyles}

  ${MediaQueries.laptop} {
    margin-top: -65px;
  }

  animation: ${liftLogoUpAndHide} 1s 4.5s linear both;
`;
