import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';
import { navMobileHeight } from 'utils/navHeight';

export const StyledPageWrapper = styled.main`
  padding-top: ${navMobileHeight}px;
  max-width: 1920px;
  margin: 0 auto;
  position: relative;

  ${MediaQueries.laptop} {
    padding-top: 0;
  }
`;
