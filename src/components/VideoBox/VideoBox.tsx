import { StyledVideoBox, StyledVideoIFrame } from './VideoBox.styled';

export interface VideoBoxProps {
  url: string;
  title: string;
  noFullScreen?: boolean;
  minWidth?: number;
}
const VideoBox: React.FC<VideoBoxProps> = ({
  url,
  title,
  noFullScreen,
  minWidth,
}) => {
  return (
    <StyledVideoBox minWidth={minWidth}>
      <StyledVideoIFrame
        title={title}
        width="100%"
        height="100%"
        src={url}
        frameBorder="0"
        allowFullScreen={!noFullScreen}
      ></StyledVideoIFrame>
    </StyledVideoBox>
  );
};

export default VideoBox;
