import { memo } from 'react';
import {
  StyledSectionDiscography,
  StyledDiscographyContent,
  StyledDiscographyInfoBox,
} from './SectionDiscography.styled';
import { StyledPageContentWrapper } from 'components/StyledPageContentWrapper/StyledPageContentWrapper';
import HeadlineSection from 'components/HeadlineSection/HeadlineSection';
import Box, {
  BoxProps,
} from 'components/TimelineDateAndInfoBox/TimelineDateAndInfoBox';

import { BoxData } from './data/BoxData';
export interface SectionDiscographyProps {}

const SectionDiscography: React.FC<SectionDiscographyProps> = () => {
  const boxList = BoxData.map((item: BoxProps) => (
    <StyledDiscographyInfoBox key={item.InfoBoxData.title}>
      <Box
        InfoBoxData={item.InfoBoxData}
        TimelineDateData={item.TimelineDateData}
      />
    </StyledDiscographyInfoBox>
  ));

  return (
    <StyledSectionDiscography id="discography">
      <StyledPageContentWrapper>
        <HeadlineSection
          titleText="discography"
          titleFirstColor="190,160,140"
          titleSecondColor="150,100,72"
          descriptionText="September 4 world heard Night Visions, the first full album. He reached the 2 position in the chart Billboard 200. The single «It's Time» took 22 th place in the Billboard Hot 100, 4th in the Billboard Alternative and Billboard Rock, and now went platinum."
        />
        <StyledDiscographyContent>{boxList}</StyledDiscographyContent>
      </StyledPageContentWrapper>
    </StyledSectionDiscography>
  );
};

export default memo(SectionDiscography);
