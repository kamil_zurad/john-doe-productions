import styled from 'styled-components';

export const StyledSocialMediaCounters = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-left: 20px;
`;
