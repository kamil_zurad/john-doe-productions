import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

export const StyledDescriptionInfo = styled.p`
  letter-spacing: 0px;
  font-size: ${({ theme }) => theme.sizes.description.fontSizeMobile}px;
  color: rgb(${({ theme }) => theme.lightModeColors.textSecondary});
  line-height: ${({ theme }) => theme.sizes.description.lineHeightMobile}px;

  ${MediaQueries.laptop} {
    font-size: ${({ theme }) => theme.sizes.description.fontSizeDesktop}px;
    line-height: ${({ theme }) => theme.sizes.description.lineHeightDesktop}px;
  }
`;
