import { memo } from 'react';
import styled, { css } from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

export const LogoStyles = css`
  width: 250px;

  ${MediaQueries.tablet} {
    width: 350px;
  }

  ${MediaQueries.laptop} {
    width: auto;
  }
`;

export const StyledHeaderShowcaseIMG = styled.img`
  display: block;
  ${LogoStyles}
`;

export default memo(StyledHeaderShowcaseIMG);
