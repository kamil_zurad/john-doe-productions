import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

export const StyledHamburgerBtn = styled.button`
  position: relative;
  width: 40px;
  height: 30px;
  border: none;
  background-color: transparent;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  cursor: pointer;
  outline: none;
  -webkit-tap-highlight-color: transparent;
  ${MediaQueries.laptop} {
    display: none;
  }
`;
