import styled from 'styled-components';

export const StyledLatterCompositionsWrapper = styled.section`
  min-height: 100vh;
  padding-top: 150px;
  padding-bottom: 150px;
`;
