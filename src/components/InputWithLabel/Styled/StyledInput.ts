import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';
export interface StyledInputProps {
  isNotValidate?: boolean;
}
export const StyledInput = styled.input<StyledInputProps>`
  padding: 10px 10px 10px 0;
  text-indent: 10px;
  width: 100%;
  border: 2px solid
    rgba(
      ${({ isNotValidate, theme }) =>
        isNotValidate ? '255,30,30' : `${theme.lightModeColors.textSecondary}`},
      0.5
    );

  &::placeholder {
    text-transform: capitalize;
  }

  ${MediaQueries.laptop} {
    min-width: 216px;
  }
`;
