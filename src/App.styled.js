import { createGlobalStyle } from 'styled-components';
import { normalize } from 'styled-normalize';
import './fonts.css';

export default createGlobalStyle`
  ${normalize}
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  body,html{
    font-family: "OpenSans", sans-serif;
  } 
  body.disableScroll {
    overflow: hidden;
  }
`;
