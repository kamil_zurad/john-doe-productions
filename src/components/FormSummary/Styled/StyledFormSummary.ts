import styled from 'styled-components';
import concertToursBG from 'assets/images/concertTours/concertToursBG.jpg';
export const StyledFormSummary = styled.div`
  width: 100%;
  height: 100%;
  background-image: url(${concertToursBG});
  padding: 15px;
  background-position: center;
`;
