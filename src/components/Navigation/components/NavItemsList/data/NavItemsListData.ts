import { NavItemType } from '../../NavItem/data/NavItemType';

export const NavItemsListData: NavItemType[] = [
  {
    label: 'about',
    sectionToScroll: 'header',
  },
  {
    label: 'discography',
    sectionToScroll: 'discography',
  },
  {
    label: 'concert tours',
    sectionToScroll: 'concertTours',
  },
  {
    label: 'Latter compositions',
    sectionToScroll: 'LatterCompositions',
  },
  {
    label: 'new tracks',
    sectionToScroll: 'newTracks',
  },
  {
    label: 'upcoming events',
    sectionToScroll: 'upcomingEvents',
  },
  {
    label: 'history',
    sectionToScroll: 'history',
  },
  {
    label: 'contact',
    sectionToScroll: 'contact',
  },
];
