import styled from 'styled-components';

export const StyledFormSummaryTitle = styled.p`
  text-transform: uppercase;
  padding: 10px 0;
  text-align: left;
  font-weight: 700;
`;
