import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';
import { StyledPageContentWrapper } from 'components/StyledPageContentWrapper/StyledPageContentWrapper';

export const StyledNavWrapper = styled(StyledPageContentWrapper)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;
  padding: 0;

  ${MediaQueries.laptop} {
    flex-wrap: nowrap;
  }
`;
