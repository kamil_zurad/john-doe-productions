import { memo } from 'react';
import {
  StyledSectionConcertTours,
  StyledConcertToursBoxWrapper,
} from './SectionConcertTours.styled';
import { StyledPageContentWrapper } from 'components/StyledPageContentWrapper/StyledPageContentWrapper';
import HeadlineSection from 'components/HeadlineSection/HeadlineSection';
import Box, {
  BoxProps,
} from 'components/TimelineDateAndInfoBox/TimelineDateAndInfoBox';
import { ConcertToursData } from './data/ConcertToursData';

export interface SectionConcertToursProps {}

const SectionConcertTours: React.FC<SectionConcertToursProps> = () => {
  const Concerts = ConcertToursData.map((item: BoxProps) => (
    <Box
      key={item.InfoBoxData.title}
      vertical={item.vertical}
      InfoBoxData={item.InfoBoxData}
      TimelineDateData={item.TimelineDateData}
    />
  ));

  return (
    <StyledSectionConcertTours id="concertTours">
      <StyledPageContentWrapper>
        <HeadlineSection
          titleText="concert tours"
          titleFirstColor="98,162,173"
          titleSecondColor="141,183,179"
          descriptionText={`Before the release of Night Visions, Imagine Dragons made appearances on American radio and television to promote their extended play, Continued Silence and debut single It's Time. The band performed "It's Time" on the July 16, 2012 airing of NBC late-night talk show The Tonight Show with Jay Leno"`}
        />
        <StyledConcertToursBoxWrapper>{Concerts}</StyledConcertToursBoxWrapper>
      </StyledPageContentWrapper>
    </StyledSectionConcertTours>
  );
};

export default memo(SectionConcertTours);
