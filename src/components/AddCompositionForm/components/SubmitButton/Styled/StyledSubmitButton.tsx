import styled from 'styled-components';
import { ButtonStyles } from 'components/Button/Styled/StyledButton';
export interface StyledButtonProps {
  marginHorizontal?: number;
  background?: any;
}
export const StyledSubmitButton = styled.button<StyledButtonProps>`
  ${ButtonStyles}
`;
