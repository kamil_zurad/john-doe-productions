import * as navHeights from './navHeight';

const theme = {
  lightModeColors: {
    textPrimary: '0,0,0',
    textSecondary: '121, 130, 133',
    textThird: '172, 172, 172',
    navBorderColor: '206, 206, 206',
  },
  sizes: {
    sectionTitle: {
      fontSizeMobile: 22,
      fontSizeDesktop: 50,
      lineHeightMobile: 30,
      lineHeightDesktop: 59,
    },
    sectionDescription: {
      fontSizeMobile: 14,
      fontSizeDesktop: 14,
      lineHeightMobile: 24,
      lineHeightDesktop: 24,
    },
    description: {
      fontSizeMobile: 14,
      fontSizeDesktop: 14,
      lineHeightMobile: 24,
      lineHeightDesktop: 24,
    },
    title: {
      fontSizeMobile: 18,
      fontSizeDesktop: 24,
      lineHeightMobile: 20,
      lineHeightDesktop: 24,
    },
    btn: {
      fontSize: 14,
      lineHeight: 24,
    },
  },
  wrapper: {
    maxWidth: 1240,
    mobilePadding: 15,
  },
  navBar: {
    mobileHeight: navHeights.navMobileHeight,
    desktopHeight: navHeights.navDesktopHeight,
    fontSize: 17,
    letterSpacing: 2,
    lineHeight: 40,
    navBg: '0, 8, 12',
  },
};

export default theme;
