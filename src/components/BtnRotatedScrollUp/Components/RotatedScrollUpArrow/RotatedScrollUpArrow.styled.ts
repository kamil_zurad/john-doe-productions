import { memo } from 'react';
import styled from 'styled-components';

const StyledRotatedScrollUpArrow = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  padding: 10px;
  cursor: pointer;
  -webkit-tap-highlight-color: transparent;
  transition: filter 0.1s linear;

  &:hover {
    filter: drop-shadow(0 0px 20px rgb(110, 110, 100));
  }
`;

export default memo(StyledRotatedScrollUpArrow);
