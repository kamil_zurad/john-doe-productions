# Mobitouch Project

> Developed by [Kamil Żurad](https://github.com/kamil-zurad)

## Technologies:

This project is created with:

- React
- Redux
- styled-components
- react-final-form
- TypeScript
- yarn
- JSON Server

## 📢 Notice

### Plugins to install in the editor:

- _ESLint_
- _Prettier_

```json
{
  "editor.formatOnSave": true,
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  }
}
```

## Setup

To run this project, install it locally using yarn:

```
$ yarn install
$ yarn start:db
$ yarn start
```

- `yarn start:db` - start server with Latter Compositions data
- `yarn start` - start website project

This project uses link to JSON Server which is pushed to Heroku. If you want to look around locally follow the instruction in src/data/API.ts file

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
