import { StyledScrollingLink } from './ScrollingLink.styled';
import { navDesktopHeight } from 'utils/navHeight';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'store/rootReducer';
import {
  mobileNavIsNotVisible,
  mobileNavIsVisible,
} from 'store/mobileNavScroll/mobileNavScroll.actions';
import { Breakpoints } from 'utils/MediaQueries';

export interface ScrollingLinkProps {
  isLinkFloorVisible: boolean;
  sectionToScroll: string | null;
  isCurrentlyTracked: boolean;
}

const ScrollingLink: React.FC<ScrollingLinkProps> = ({
  isLinkFloorVisible,
  sectionToScroll,
  children,
  isCurrentlyTracked,
}) => {
  const dispatch = useDispatch();
  const isMobileNavVisible = useSelector(
    (state: RootState) => state.mobileNavScroll.isMobileNavVisible,
  );
  const handleOnClick = () => {
    if (window.innerWidth >= Breakpoints.laptop || sectionToScroll === null)
      return;

    document.body.classList.remove('disableScroll');
    if (isMobileNavVisible) {
      dispatch(mobileNavIsNotVisible());
    } else {
      dispatch(mobileNavIsVisible());
    }
  };

  return (
    <StyledScrollingLink
      $isLinkFloorVisible={isLinkFloorVisible}
      activeClass="active"
      to={sectionToScroll!}
      spy={true}
      smooth={true}
      offset={-navDesktopHeight + 2}
      duration={500}
      onClick={handleOnClick}
      $isCurrentlyTracked={isCurrentlyTracked}
    >
      {children}
    </StyledScrollingLink>
  );
};

export default ScrollingLink;
