import { useEffect } from 'react';
import { createPortal } from 'react-dom';
import { useHistory } from 'react-router-dom';
import {
  StyledModalWrapper,
  StyledModalContent,
  StyledModalCloseIcon,
} from './Modal.styled';
import { Breakpoints } from 'utils/MediaQueries';
export interface ModalProps {}

const Modal: React.FC<ModalProps> = ({ children }) => {
  const history = useHistory();

  const getBackAndEnableScroll = () => {
    history.goBack();
    document.body.classList.remove('disableScroll');
  };

  const handleClose = () => {
    getBackAndEnableScroll();
  };

  const disableScrollOnMobile = () => {
    if (window.innerWidth < Breakpoints.laptop) {
      document.body.classList.add('disableScroll');
    }
  };

  useEffect(() => {
    disableScrollOnMobile();
  }, []);

  return createPortal(
    <StyledModalWrapper onClick={handleClose}>
      <StyledModalContent onClick={(e: any) => e.stopPropagation()}>
        <StyledModalCloseIcon onClick={getBackAndEnableScroll}>
          &times;
        </StyledModalCloseIcon>
        {children}
      </StyledModalContent>
    </StyledModalWrapper>,
    document.getElementById('modal')!,
  );
};

export default Modal;
