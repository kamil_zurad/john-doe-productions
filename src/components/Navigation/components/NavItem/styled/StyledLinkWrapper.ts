import styled from 'styled-components';

export const StyledLinkWrapper = styled.span`
  width: 100%;
  height: auto;
  display: flex;
  justify-content: center;
  align-items: center;
`;
