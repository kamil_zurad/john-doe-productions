import { useRef, memo } from 'react';
import {
  StyledHeader,
  StyledHeaderShowcaseWrapper,
  StyledScrollDownBtnWrapper,
} from './SectionHeader.styled';

import BtnScrollDownArrow from 'components/BtnScrollDownArrow/BtnScrollDownArrow';

import HeaderShowcase from './components/HeaderShowcase/HeaderShowcase';
export interface SectionHeaderProps {}

const SectionHeader: React.FC<SectionHeaderProps> = () => {
  const headerRef = useRef(null);

  return (
    <StyledHeader ref={headerRef} id="header">
      <StyledHeaderShowcaseWrapper>
        <HeaderShowcase headerRef={headerRef} />
        <StyledScrollDownBtnWrapper>
          <BtnScrollDownArrow headerRef={headerRef} />
        </StyledScrollDownBtnWrapper>
      </StyledHeaderShowcaseWrapper>
    </StyledHeader>
  );
};

export default memo(SectionHeader);
