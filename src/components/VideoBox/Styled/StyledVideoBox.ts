import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

export interface StyledVideoBoxProps {
  minWidth?: number;
}

export const StyledVideoBox = styled.div<StyledVideoBoxProps>`
  flex-basis: 50%;

  ${MediaQueries.laptop} {
    min-width: ${({ minWidth }) => (minWidth ? `${minWidth}px` : `unset`)};
  }
`;
