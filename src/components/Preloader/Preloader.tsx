import {
  StyledPreloader,
  StyledPreloaderLogo,
  StyledPreloaderShowcase,
  StyledPreloaderProgressBar,
  StyledProgressBarTitle,
  StyledProgressBarTitleWrapper,
} from './Preloader.styled';
import { createPortal } from 'react-dom';
import headerShowcaseLogo from 'assets/images/header/headerLogo.png';
import PreloaderLine from './components/PreloaderLine/PreloaderLine';
import { useEffect, useState } from 'react';

export interface PreloaderProps {}

const preloaderLinesDistances = [16.66, 33.33, 50, 66.66, 83.33];

const Preloader: React.FC<PreloaderProps> = () => {
  const preloaderLines = preloaderLinesDistances.map((lineDistance) => (
    <PreloaderLine key={lineDistance} distanceToLeftSide={lineDistance} />
  ));

  const [isPreloaderVisible, setIsPreloaderVisible] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setIsPreloaderVisible(false);
    }, 7500);
  }, []);

  const loadingText = 'loading...';

  return createPortal(
    <StyledPreloader isPreloaderVisible={isPreloaderVisible}>
      {preloaderLines}
      <StyledPreloaderShowcase>
        <StyledPreloaderLogo src={headerShowcaseLogo} alt="preloader logo" />
        <StyledProgressBarTitleWrapper>
          <StyledProgressBarTitle data-loading-text={loadingText}>
            {loadingText}
          </StyledProgressBarTitle>
        </StyledProgressBarTitleWrapper>
        <StyledPreloaderProgressBar />
      </StyledPreloaderShowcase>
    </StyledPreloader>,
    document.getElementById('preloader')!,
  );
};

export default Preloader;
