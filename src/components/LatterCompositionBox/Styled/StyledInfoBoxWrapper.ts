import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

export interface StyledInfoBoxWrapperProps {
  isReversed?: boolean;
}

export const StyledInfoBoxWrapper = styled.div<StyledInfoBoxWrapperProps>`
  flex-basis: 50%;
  padding: 0;
  background-color: rgb(245, 246, 246);
  position: relative;

  ${MediaQueries.laptop} {
    padding: 30px;
  }

  &::after {
    content: '';
    position: absolute;
    top: 100%;
    left: 50%;
    transform: translate(-50%, 0%);
    width: 0;
    height: 0;
    border-left: 15px solid transparent;
    border-right: 15px solid transparent;
    border-top: 25px solid rgb(245, 246, 246);

    ${MediaQueries.laptop} {
      top: 50%;
      left: ${({ isReversed }) => (isReversed ? 'unset' : 0)};
      right: ${({ isReversed }) => (isReversed ? 0 : 'unset')};
      transform: translate(
        ${({ isReversed }) => (isReversed ? '100%' : '-100%')},
        -50%
      );
      border-top: 15px solid transparent;
      border-left: ${({ isReversed }) =>
        isReversed ? '25px solid rgb(245, 246, 246)' : `unset`};
      border-right: ${({ isReversed }) =>
        isReversed ? 'unset' : `20px solid rgb(245, 246, 246)`};
      border-bottom: 15px solid transparent;
    }
  }
`;
