import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';
export interface StyledTitleSectionWrapperProps {
  noLineThrought?: boolean;
}

export const StyledTitleSectionWrapper = styled.h2<StyledTitleSectionWrapperProps>`
  position: relative;
  width: max-content;
  padding: 0;

  ${MediaQueries.tablet} {
    padding: 0 60px;
  }

  &::before {
    content: ${({ noLineThrought }) => (noLineThrought ? '' : '""')};
    position: absolute;
    top: 50%;
    left: 50%;
    height: 1px;
    width: 100%;
    background-color: rgb(0, 0, 0);
    transform: translate(-50%, -50%);
  }
`;
