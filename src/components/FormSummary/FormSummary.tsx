import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'store/rootReducer';
import { ILatterCompositionForm } from 'components/AddCompositionForm/data/ILatterCompositionForm';
import { useHistory } from 'react-router-dom';
import {
  pushPendingCompositionToServer,
  clearPendingComposition,
} from 'store/latterCompositions/latterCompositions.actions';
import {
  StyledFormSummary,
  StyledFormSummaryTitle,
  StyledButtonsWrapper,
} from './FormSummary.styled';
import buttonBG from 'assets/images/Btns/btnBG.jpg';
import buttonBG2 from 'assets/images/Btns/btnBG2.jpg';
import { ButtonVariants } from 'components/OutsidePageLink/OutsidePageLink';
import { SocialMediaCounterIcons } from 'components/SocialMediaCounter/SocialMediaCounter';
import LatterCompositionBox, {
  LatterCompositionBoxProps,
} from 'components/LatterCompositionBox/LatterCompositionBox';
import Button from 'components/Button/Button';
export interface FormSummaryProps {
  backToForm: () => void;
}

const FormSummary: React.FC<FormSummaryProps> = ({ backToForm }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const dataSummary: ILatterCompositionForm = useSelector(
    (state: RootState) => state.latterCompositions.compositionPendingToBeAdded,
  );

  const handleAddItem = () => {
    document.body.classList.remove('disableScroll');
    history.push({ pathname: '/' });
    dispatch(pushPendingCompositionToServer(dataSummary));
    dispatch(clearPendingComposition());
  };

  const embedLink = dataSummary.link.replace('watch?v=', 'embed/');
  const dateFrom1970AsNumber = Date.parse(dataSummary.date);
  const finalIndexBeforeWhichCutTexts = 28;

  const getTextToSummaryDisplay = (text: string): string => {
    let textToSummary;
    if (text.length >= finalIndexBeforeWhichCutTexts) {
      textToSummary = `${text.slice(0, finalIndexBeforeWhichCutTexts)}...`;
    } else {
      textToSummary = text;
    }
    return textToSummary;
  };

  const compositionSummary: LatterCompositionBoxProps = {
    BoxData: {
      vertical: true,
      InfoBoxData: {
        title: getTextToSummaryDisplay(dataSummary.title),
        description: getTextToSummaryDisplay(dataSummary.description),
        btnData: {
          label: 'Visit The iTunes',
          to: dataSummary.link,
          background: '',
          btnVariant: ButtonVariants.SECONDARY,
        },
        socialMediaItemsData: [
          {
            likesNumber: 12994197,
            icon: SocialMediaCounterIcons.COMMENT,
          },
          {
            likesNumber: 4354,
            icon: SocialMediaCounterIcons.COMMENT,
          },
          {
            likesNumber: 345,
            icon: SocialMediaCounterIcons.COMMENT,
          },
        ],
      },
      TimelineDateData: {
        timelineBottomSpace: 30,
        dateTextData: {
          date: dateFrom1970AsNumber,
          showFullDate: true,
        },
        title: getTextToSummaryDisplay(dataSummary.dataTitle),
      },
    },
    VideoBoxData: {
      title: 'asd',
      url: embedLink,
      noFullScreen: false,
      minWidth: 550,
    },
  };

  return (
    <StyledFormSummary>
      <StyledFormSummaryTitle>summary</StyledFormSummaryTitle>
      <LatterCompositionBox {...compositionSummary} />
      <StyledButtonsWrapper>
        <Button
          onClickFunction={backToForm}
          background={buttonBG}
          marginRight={10}
        >{`< Back`}</Button>
        <Button onClickFunction={handleAddItem} background={buttonBG2}>
          {`accept`}
        </Button>
      </StyledButtonsWrapper>
    </StyledFormSummary>
  );
};

export default FormSummary;
