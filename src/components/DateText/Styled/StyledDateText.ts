import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

export const StyledDateText = styled.span`
  font-size: ${({ theme }) => theme.sizes.description.fontSizeMobile}px;
  line-height: ${({ theme }) => theme.sizes.description.lineHeightMobile}px;
  color: rgba(${({ theme }) => theme.lightModeColors.textPrimary}, 0.65);

  ${MediaQueries.laptop} {
    font-size: ${({ theme }) => theme.sizes.description.fontSizeDesktop}px;
    line-height: ${({ theme }) => theme.sizes.description.lineHeightDesktop}px;
  }
`;
