import { StyledPageLink } from './PageLink.styled';
import { ButtonProps } from 'components/OutsidePageLink/OutsidePageLink';
import { ButtonVariants } from 'components/OutsidePageLink/OutsidePageLink';
import { StyledButtonArrow } from 'components/OutsidePageLink/OutsidePageLink.styled';

export interface PageLinkProps extends ButtonProps {
  onClickFunction?: any;
}

const PageLink: React.FC<PageLinkProps> = ({
  to,
  label,
  onClickFunction,
  background,
  btnVariant,
}) => {
  const btnArrow = btnVariant === ButtonVariants.SECONDARY && (
    <StyledButtonArrow>{`>`}</StyledButtonArrow>
  );

  return (
    <StyledPageLink to={to} onClick={onClickFunction} background={background}>
      {label}
      {btnArrow}
    </StyledPageLink>
  );
};

export default PageLink;
