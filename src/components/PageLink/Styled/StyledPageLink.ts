import styled from 'styled-components';
import { Link } from 'react-router-dom';
import {
  StyledButtonStyles,
  StyledButtonProps,
} from 'components/OutsidePageLink/Styled/StyledButton';

export const StyledPageLink = styled(Link)<StyledButtonProps>`
  ${StyledButtonStyles}
`;
