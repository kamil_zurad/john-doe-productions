import { ThemeProvider } from 'styled-components';
import theme from 'utils/theme';
import CreateGlobalStyle from './App.styled';
import Routes from './Routes';
function App() {
  return (
    <ThemeProvider theme={theme}>
      <CreateGlobalStyle />

      <div className="App">
        <Routes />
      </div>
    </ThemeProvider>
  );
}

export default App;
