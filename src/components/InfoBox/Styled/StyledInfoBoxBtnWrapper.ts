import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';
export const StyledInfoBoxBtnWrapper = styled.div`
  margin-top: 20px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;

  ${MediaQueries.laptop} {
    flex-direction: row;
  }
`;
