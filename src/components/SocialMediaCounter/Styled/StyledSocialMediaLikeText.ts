import styled from 'styled-components';

export const StyledSocialMediaLikeText = styled.span`
  margin: 0 10px;
  font-size: 12px;
  color: rgb(${({ theme }) => theme.lightModeColors.textThird});
`;
