import styled from 'styled-components';

export const StyledModalCloseIcon = styled.div`
  position: absolute;
  right: 5px;
  top: 3px;
  cursor: pointer;
  font-size: 25px;
  font-weight: bold;
`;
