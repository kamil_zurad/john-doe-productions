import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

export const StyledDescriptionSection = styled.p`
  text-align: center;
  letter-spacing: 2px;
  font-size: ${({ theme }) => theme.sizes.sectionDescription.fontSizeMobile}px;
  color: rgb(${({ theme }) => theme.lightModeColors.textSecondary});
  line-height: ${({ theme }) =>
    theme.sizes.sectionDescription.lineHeightMobile}px;

  ${MediaQueries.laptop} {
    font-size: ${({ theme }) =>
      theme.sizes.sectionDescription.fontSizeDesktop}px;
    line-height: ${({ theme }) =>
      theme.sizes.sectionDescription.lineHeightDesktop}px;
  }
`;
