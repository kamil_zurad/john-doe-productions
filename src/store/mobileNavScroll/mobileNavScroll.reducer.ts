import { mobileNavScrollTypes } from './mobileNavScroll.constants';
import { mobileNavScrollActionType } from './mobileNavScroll.actions';

const INITIAL_STATE = {
  isMobileNavShowingSmoothly: false,
  isMobileNavVisible: false,
};

function mobileNavScroll(
  state = INITIAL_STATE,
  action: mobileNavScrollActionType,
) {
  switch (action.type) {
    case mobileNavScrollTypes.MOBILE_NAV_SHOWS_SMOOTHLY:
      return {
        ...state,
        isMobileNavShowingSmoothly: true,
      };
    case mobileNavScrollTypes.MOBILE_NAV_SHOWS_INSTANTLY:
      return {
        ...state,
        isMobileNavShowingSmoothly: false,
      };
    case mobileNavScrollTypes.MOBILE_NAV_IS_VISIBLE:
      return {
        ...state,
        isMobileNavVisible: true,
      };
    case mobileNavScrollTypes.MOBILE_NAV_IS_NOT_VISIBLE:
      return {
        ...state,
        isMobileNavVisible: false,
      };
    default:
      return state;
  }
}

export default mobileNavScroll;
