export interface ILatterCompositionForm {
  title: string;
  description: string;
  link: string;
  dataTitle: string;
  date: string;
}
