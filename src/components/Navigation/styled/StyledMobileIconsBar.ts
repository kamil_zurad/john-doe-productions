import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

export const StyledMobileIconsBar = styled.div`
  position: relative;
  z-index: 1;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  width: 100%;
  height: 100%;
  padding: 0 10px;
  background-color: rgba(229, 230, 230, 1);

  ${MediaQueries.laptop} {
    padding-left: 0;
    padding-right: 0;
    width: auto;
    background-color: transparent;
  }
`;
