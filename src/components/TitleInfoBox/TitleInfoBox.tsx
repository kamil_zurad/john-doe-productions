import { StyledTitleInfoBox } from './TitleInfoBox.styled';

export interface TitleInfoBoxProps {}

const TitleInfoBox: React.FC<TitleInfoBoxProps> = ({ children }) => {
  return <StyledTitleInfoBox>{children}</StyledTitleInfoBox>;
};

export default TitleInfoBox;
