import styled from 'styled-components';
import concertToursBG from 'assets/images/concertTours/concertToursBG.jpg';

export const StyledSectionConcertTours = styled.section`
  padding-top: 180px;
  padding-bottom: 200px;
  background-image: url(${concertToursBG});
  background-position-x: center;
  background-position-y: bottom;
  background-repeat: no-repeat;
`;
