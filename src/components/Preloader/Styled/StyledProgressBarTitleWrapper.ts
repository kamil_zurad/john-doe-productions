import styled from 'styled-components';

export const StyledProgressBarTitleWrapper = styled.div`
  display: flex;
  justify-content: center;
`;
