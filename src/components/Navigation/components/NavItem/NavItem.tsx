import { StyledNavListItem } from './NavItem.styled';
import { useEffect, useState } from 'react';
import { Breakpoints } from 'utils/MediaQueries';
import ScrollingLink from '../ScrollingLink/ScrollingLink';
import ViewEventConditionals from 'helpers/ViewEventConditionals';
import { navDesktopHeight } from 'utils/navHeight';

export interface NavItemProps {
  label: string;
  isLast: boolean;
  sectionToScroll?: string | null;
}

const NavItem: React.FC<NavItemProps> = ({
  label,
  isLast,
  sectionToScroll,
}) => {
  const [isCurrentlyTracked, setIsCurrentlyTracked] = useState(false);
  const trackNavItem = (sectionToScroll: any) => {
    if (sectionToScroll === null || window.innerWidth < Breakpoints.laptop)
      return;
    if (
      ViewEventConditionals.isSectionVisibleToTrackNavItem(
        sectionToScroll,
        navDesktopHeight,
      )
    ) {
      setIsCurrentlyTracked(true);
    } else {
      setIsCurrentlyTracked(false);
    }
  };
  useEffect(() => {
    const sectionToBeScrolledTo = document.getElementById(`${sectionToScroll}`);
    if (sectionToBeScrolledTo === null) return;
    trackNavItem(sectionToBeScrolledTo);
    window.addEventListener('scroll', () =>
      trackNavItem(sectionToBeScrolledTo),
    );
    return () => {
      if (sectionToBeScrolledTo === null) return;
      window.removeEventListener('scroll', trackNavItem);
    };
  }, [sectionToScroll]);

  const [isLinkFloorVisible, setisLinkFloorVisible] = useState(false);

  const handleLinkFloorVisibility = () => {
    setisLinkFloorVisible(!isLinkFloorVisible);
  };

  const handleLinkHoverOnDesktop = () => {
    if (window.innerWidth < Breakpoints.laptop) return;
    handleLinkFloorVisibility();
  };

  return (
    <StyledNavListItem
      onMouseEnter={handleLinkHoverOnDesktop}
      onMouseLeave={handleLinkHoverOnDesktop}
      isLast={isLast}
    >
      {sectionToScroll && (
        <ScrollingLink
          isLinkFloorVisible={isLinkFloorVisible}
          sectionToScroll={sectionToScroll}
          isCurrentlyTracked={isCurrentlyTracked}
        >
          {label}
        </ScrollingLink>
      )}
    </StyledNavListItem>
  );
};

export default NavItem;
