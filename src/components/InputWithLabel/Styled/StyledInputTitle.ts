import styled from 'styled-components';

export const StyledInputTitle = styled.label`
  text-transform: capitalize;
  font-weight: bold;
  margin-bottom: 3px;
`;
