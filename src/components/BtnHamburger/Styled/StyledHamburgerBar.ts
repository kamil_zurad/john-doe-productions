import styled, { css } from 'styled-components';

const commonBarsStyles = css`
  display: block;
  height: 6px;
  background-color: rgb(${({ theme }) => theme.lightModeColors.textPrimary});
  align-self: flex-end;
  width: 100%;
  border-radius: 5px;
  transition: transform 0.3s cubic-bezier(0.39, 0.99, 0.18, 1.35),
    opacity 0.2s linear;
`;

export interface StyledHamburgerBarProps {
  isBtnClicked: boolean;
}

export const StyledHamburgerTopBar = styled.span<StyledHamburgerBarProps>`
  ${commonBarsStyles}
  transform: rotate(${({ isBtnClicked }) => (isBtnClicked ? 36 : 0)}deg);
  transform-origin: left;
`;

export const StyledHamburgerMidBar = styled.span<StyledHamburgerBarProps>`
  ${commonBarsStyles}
  transform: translateX(${({ isBtnClicked }) => (isBtnClicked ? -100 : 0)}%);
  opacity: ${({ isBtnClicked }) => (isBtnClicked ? 0 : 1)};
`;

export const StyledHamburgerBotBar = styled.span<StyledHamburgerBarProps>`
  ${commonBarsStyles}
  transform: rotate(${({ isBtnClicked }) => (isBtnClicked ? -36 : 0)}deg);
  transform-origin: left;
`;
