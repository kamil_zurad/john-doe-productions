import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';
export const StyledAddCompositionBtnWrapper = styled.div`
  margin-top: 50px;
  display: flex;
  justify-content: center;

  ${MediaQueries.laptop} {
    justify-content: flex-end;
  }
`;
