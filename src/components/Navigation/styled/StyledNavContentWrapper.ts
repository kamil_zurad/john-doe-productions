import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

import { connect } from 'react-redux';

export interface StyledNavContentWrapperProps {
  isNavShowingSmoothly?: boolean;
  isMobileNavVisible?: boolean;
}

const NavContentWrapper = styled.div<StyledNavContentWrapperProps>`
  width: 100%;
  min-height: calc(100vh - ${({ theme }) => theme.navBar.mobileHeight}px);
  background-color: rgba(229, 230, 230, 1);
  padding: 10px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  transform: translateY(
    ${({ isMobileNavVisible }) => (isMobileNavVisible ? '0' : '-100%')}
  );
  transform-origin: top;
  transition: ${({ isNavShowingSmoothly }) =>
    isNavShowingSmoothly ? 'transform 0.3s ease-in-out' : 'none'};

  ${MediaQueries.laptop} {
    background-color: transparent;
    min-height: unset;
    transform: translateY(0);
    padding: 0;
    flex-direction: row;
    justify-content: space-between;
  }
`;

export const StyledNavContentWrapper = connect((state: any) => {
  return {
    isNavShowingSmoothly: state.mobileNavScroll.isMobileNavShowingSmoothly,
    isMobileNavVisible: state.mobileNavScroll.isMobileNavVisible,
  };
}, null)(NavContentWrapper);
