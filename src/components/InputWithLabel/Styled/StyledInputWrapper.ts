import styled from 'styled-components';

export interface StyledInputWrapperProps {
  marginBottom?: number;
}

export const StyledInputWrapper = styled.div<StyledInputWrapperProps>`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
  margin-bottom: ${({ marginBottom }) => (marginBottom ? marginBottom : 10)}px;
`;
