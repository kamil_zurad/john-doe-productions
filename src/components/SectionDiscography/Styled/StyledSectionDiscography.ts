import styled from 'styled-components';
import discographyBG from 'assets/images/discography/discographyBG.jpg';

export const StyledSectionDiscography = styled.section`
  min-height: 100vh;
  padding-top: 70px;
  padding-bottom: 400px;
  background-image: url(${discographyBG});
  background-position-x: center;
  background-position-y: bottom;
  background-repeat: no-repeat;
`;
