import { StyledDateText } from './DateText.styled';

export interface DateTextProps {
  date: number;
  showFullDate?: boolean;
}

const DateText: React.FC<DateTextProps> = ({ date, showFullDate }) => {
  const dateToDisplay = new Date(date);

  const addZeroBefore = (date: number): string => {
    return date < 9 ? `0${date}` : `${date}`;
  };

  const getFullDate = () => {
    return `${addZeroBefore(dateToDisplay!.getDate())}.${addZeroBefore(
      dateToDisplay!.getMonth() + 1,
    )}.${dateToDisplay!.getFullYear()}`;
  };

  const getDateToDisplay = () => {
    return showFullDate
      ? `${getFullDate()}`
      : `${dateToDisplay!.getFullYear()}`;
  };

  return <StyledDateText>{getDateToDisplay()}</StyledDateText>;
};

export default DateText;
