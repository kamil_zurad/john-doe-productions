import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

export const StyledVideoIFrame = styled.iframe`
  width: 100%;

  ${MediaQueries.laptop} {
    flex-basis: 50%;
    height: 100%;
  }
`;
