import styled, { keyframes } from 'styled-components';

const showLoadingBar = keyframes`
  100%{
    transform: scaleX(1);
  }
  `;

export const FadeOut = keyframes`
    100%{
      opacity: 0;
    }
  `;

export const StyledPreloaderProgressBar = styled.div`
  position: relative;
  width: 100%;
  height: 5px;
  background-color: rgba(255, 255, 255, 0.4);
  transform: translateY(15px);
  animation: ${FadeOut} 1s 4.5s linear forwards;

  &::after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(255, 255, 255, 0.7);
    transform: scaleX(0);
    transform-origin: left;

    box-shadow: 0px 0px 13px 0px rgba(255, 255, 255, 0.8);
    animation: ${showLoadingBar} 3s 0.5s steps(8, jump-end) both;
  }
`;
