export { StyledModalWrapper } from './Styled/StyledModalWrapper';
export { StyledModalContent } from './Styled/StyledModalContent';
export { StyledModalCloseIcon } from './Styled/StyledModalCloseIcon';
