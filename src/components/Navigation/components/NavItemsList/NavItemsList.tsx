import NavItem from '../NavItem/NavItem';
import { StyledNavItemsList } from './NavItemsList.styled';
import { NavItemsListData } from './data/NavItemsListData';

export interface NavItemsListProps {}

const NavItemsList: React.FC<NavItemsListProps> = () => {
  const NavItems = NavItemsListData.map((item, itemIndex) => (
    <NavItem
      key={item.label}
      isLast={itemIndex + 1 === NavItemsListData.length}
      sectionToScroll={item.sectionToScroll ? item.sectionToScroll : null}
      label={item.label}
    ></NavItem>
  ));

  return <StyledNavItemsList>{NavItems}</StyledNavItemsList>;
};

export default NavItemsList;
