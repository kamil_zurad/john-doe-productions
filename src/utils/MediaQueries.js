export const Breakpoints = {
  tablet: 768,
  laptop: 1024,
  desktop: 1366,
  fullHD: 1600,
  aboveFullHD: 1921,
};

const MediaQueries = {
  tablet: `@media(min-width: ${Breakpoints.tablet}px)`,
  laptop: `@media(min-width: ${Breakpoints.laptop}px)`,
  desktop: `@media(min-width: ${Breakpoints.desktop}px)`,
  fullHD: `@media(min-width: ${Breakpoints.fullHD}px)`,
  aboveFullHD: `@media(min-width: ${Breakpoints.aboveFullHD}px)`,
};

export default MediaQueries;
