import {
  StyledTimelineDateWrapper,
  StyledTimelineDateHeader,
  StyledTimelineDateTitle,
} from './TimelineDate.styled';

import DateText, { DateTextProps } from 'components/DateText/DateText';
export interface TimelineDateProps {
  dateTextData: DateTextProps;
  title: string;
  timelineBottomSpace?: number;
}

const TimelineDate: React.FC<TimelineDateProps> = ({
  title,
  dateTextData,
  timelineBottomSpace,
}) => {
  return (
    <StyledTimelineDateWrapper timelineBottomSpace={timelineBottomSpace}>
      <StyledTimelineDateHeader>
        <DateText {...dateTextData} />
      </StyledTimelineDateHeader>
      <StyledTimelineDateTitle> {title}</StyledTimelineDateTitle>
    </StyledTimelineDateWrapper>
  );
};

export default TimelineDate;
