import { BoxProps } from 'components/TimelineDateAndInfoBox/TimelineDateAndInfoBox';
import { ButtonVariants } from 'components/OutsidePageLink/OutsidePageLink';

export const ConcertToursData: BoxProps[] = [
  {
    vertical: true,
    InfoBoxData: {
      title: `Sam Feldt ft. Kimberly Anne - Show Me Love (EDX's Indian Summer Remix)`,
      description: `"Radioactive" is a song recorded by American rock band Imagine Dragons for their major-label debut EP Continued Silence and later on their debut studio album, Night Visions (2012), as the opening track. "Radioactive" was first sent to modern rock radio on October 29, 2012,[1] and released to contemporary radio on April 9, 2013. Musically, "Radioactive" is an alternative rock song with elements of electronic rock and contains cryptic lyrics about apocalyptic and revolutionist themes.`,
      btnData: {
        label: 'Buy online',
        to: 'https://www.youtube.com/watch?v=4KGsgpFiswQ',
        background: '',
        btnVariant: ButtonVariants.SECONDARY,
      },
    },
    TimelineDateData: {
      timelineBottomSpace: 30,
      dateTextData: {
        date: 1290096094197,
      },
      title: 'indian sammer',
    },
  },
];
