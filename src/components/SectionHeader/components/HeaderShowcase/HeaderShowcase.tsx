import { memo } from 'react';
import { StyledHeaderShowcaseWrapper } from './HeaderShowcase.styled';
import StyledHeaderShowcaseIMG from './Styled/StyledHeaderImage';
import headerShowcaseLogo from 'assets/images/header/headerLogo.png';
import { useCallback, useEffect, useState } from 'react';
import ViewEventConditionals from 'helpers/ViewEventConditionals';
import { Breakpoints } from 'utils/MediaQueries';
export interface HeaderShowcaseProps {
  headerRef: any;
}

const HeaderShowcase: React.FC<HeaderShowcaseProps> = ({ headerRef }) => {
  const [showcaseOffsetY, setShowcaseOffsetY] = useState(0);

  const handleScroll = () => setShowcaseOffsetY(window.pageYOffset * 0.5);

  const parallaxShowcaseImage = useCallback(() => {
    if (window.innerWidth < Breakpoints.laptop) {
      setShowcaseOffsetY(0);
    } else {
      if (!ViewEventConditionals.isSectionVisible(headerRef.current)) return;
      handleScroll();
    }
  }, [headerRef]);

  useEffect(() => {
    window.addEventListener('scroll', parallaxShowcaseImage);
    return () => {
      window.removeEventListener('scroll', parallaxShowcaseImage);
    };
  }, [parallaxShowcaseImage]);

  return (
    <StyledHeaderShowcaseWrapper>
      <StyledHeaderShowcaseIMG
        src={headerShowcaseLogo}
        alt="showcase"
        style={{
          transform: `translateY(-${showcaseOffsetY}px)`,
        }}
      />
    </StyledHeaderShowcaseWrapper>
  );
};

export default memo(HeaderShowcase);
