import { RefObject } from 'react';
import {
  StyledScrollDownArrowBtn,
  StyledArrow,
} from './BtnScrollDownArrow.styled';
import { navDesktopHeight } from 'utils/navHeight';
import { Breakpoints } from 'utils/MediaQueries';
export interface ScrollDownBtnProps {
  headerRef: RefObject<HTMLElement>;
}

const BtnScrollDownArrow: React.FC<ScrollDownBtnProps> = ({ headerRef }) => {
  const handleOnCLick = () => {
    const topScroll =
      window.innerWidth >= Breakpoints.laptop
        ? headerRef.current!.clientHeight - navDesktopHeight
        : headerRef.current!.clientHeight;

    window.scrollTo({
      left: 0,
      top: topScroll + 2,
      behavior: 'smooth',
    });
  };
  return (
    <StyledScrollDownArrowBtn onClick={handleOnCLick}>
      <StyledArrow />
    </StyledScrollDownArrowBtn>
  );
};

export default BtnScrollDownArrow;
