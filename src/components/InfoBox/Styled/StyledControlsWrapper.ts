import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

export const StyledControlsWrapper = styled.div`
  margin: 20px 0;
  ${MediaQueries.laptop} {
    margin: 0;
  }
`;
