import { StyledDescriptionSection } from './DescriptionSection.styled';

export interface DescriptionSectionProps {
  text: string;
}

const DescriptionSection: React.FC<DescriptionSectionProps> = ({ text }) => {
  return <StyledDescriptionSection>{text}</StyledDescriptionSection>;
};

export default DescriptionSection;
