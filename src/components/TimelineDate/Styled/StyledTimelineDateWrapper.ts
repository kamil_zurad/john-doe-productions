import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';

export interface StyledTimelineDateWrapperProps {
  timelineBottomSpace?: number;
}

export const StyledTimelineDateWrapper = styled.div<StyledTimelineDateWrapperProps>`
  width: 100%;
  margin-bottom: ${({ theme, timelineBottomSpace }) =>
    timelineBottomSpace
      ? timelineBottomSpace
      : theme.sizes.title.fontSizeMobile}px;

  ${MediaQueries.laptop} {
    margin-bottom: ${({ timelineBottomSpace }) =>
      timelineBottomSpace ? timelineBottomSpace : 0}px;
    max-width: 535px;
    flex-grow: 1;
    padding-right: 20px;
  }

  ${MediaQueries.desktop} {
    padding-right: 0;
  }
`;
