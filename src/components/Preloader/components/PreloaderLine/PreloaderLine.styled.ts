import styled from 'styled-components';

export interface StyledPreloaderLineProps {
  distanceToLeftSide: number;
}

export const StyledPreloaderLine = styled.span<StyledPreloaderLineProps>`
  position: absolute;
  display: block;
  top: 0;
  left: ${({ distanceToLeftSide }) =>
    distanceToLeftSide ? distanceToLeftSide : 0}%;
  height: 100vh;
  width: 1px;
  background-color: rgba(255, 255, 255, 0.2);
`;
