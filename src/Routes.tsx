import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Homepage } from './pages/_pages';
export interface RouterProps {}

const Routes: React.FC<RouterProps> = () => {
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <Switch>
        <Route path="/" exact={true} component={Homepage} />
        <Route path="/addComposition" exact={true} component={Homepage} />
      </Switch>
    </Router>
  );
};

export default Routes;
