import styled from 'styled-components';

export const StyledSocialMediaCounter = styled.div`
  display: flex;
  padding: 0 10px;
`;
