import styled, { keyframes } from 'styled-components';

const fadeInPreloaderBackground = keyframes`
  100% {
    transform: translateY(-100%);
  }
`;

export interface StyledPreloaderProps {
  isPreloaderVisible: boolean;
}

export const StyledPreloader = styled.div<StyledPreloaderProps>`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 11;
  width: 100%;
  height: 100vh;
  display: ${({ isPreloaderVisible }) =>
    isPreloaderVisible ? 'flex' : 'none'};
  align-items: center;
  justify-content: center;
  background-color: black;
  animation: ${fadeInPreloaderBackground} 1s 6s linear forwards;
`;
