import { BoxProps } from 'components/TimelineDateAndInfoBox/TimelineDateAndInfoBox';
import buttonBG from 'assets/images/Btns/btnBG.jpg';
import buttonBG2 from 'assets/images/Btns/btnBG2.jpg';
import buttonBG3 from 'assets/images/Btns/btnBG3.jpg';

export const BoxData: BoxProps[] = [
  {
    InfoBoxData: {
      title: `Hell and Silence is an EP by Las Vegas rock group`,
      description: `Hell and Silence is an EP by Las Vegas rock group , released in March 2010 in the United States. It was recorded at Battle Born Studios. All songs were written by Imagine Dragons and self-produced. The EP was in part mixed by Grammy nominated engineer Mark Needham.
      To promote the album the band performed five shows at SXSW 2010 including at the BMI Official Showcase.While at SXSW they were endorsed by Blue Microphones. They also toured the western United States with Nico Vega and Saint Motel. They also performed at Bite of Las Vegas Festival 2010, New Noise Music Festival, Neon Reverb Festival, and Fork Fest.`,
      btnData: {
        label: 'PLAY',
        to: 'https://www.youtube.com/watch?v=dhv9mV04hdY',
        background: buttonBG,
      },
    },
    TimelineDateData: {
      timelineBottomSpace: 30,
      dateTextData: {
        date: 1290096094197,
      },
      title: 'Hell and Silence',
    },
  },
  {
    InfoBoxData: {
      title: `Night Visions is the debut studio album by American rock band`,
      description: `It was released on September 4, 2012 through Interscope Records. The extended track was released on February 12, 2013, adding three more songs. Recorded between 2010 and 2012, the album was primarily produced by the band themselves, as well as English hip-hop producer Alex da Kid and Brandon Darner from the American indie rock group The Envy Corps. It was mastered by Joe LaPorta. According to frontman Dan Reynolds, the album took three years to finish ...
      of the album's tracks being previously released on multiple EPs. Musically, Night Visions exhibits influences of folk, hip hop and pop.[2]`,
      btnData: {
        label: 'PLAY',
        to: 'https://www.youtube.com/watch?v=gp8rC2-zKZU',
        background: buttonBG2,
      },
    },
    TimelineDateData: {
      timelineBottomSpace: 30,
      dateTextData: {
        date: 1290096094197,
      },
      title: 'Night Visions',
    },
  },
  {
    InfoBoxData: {
      title: `The album was recorded during 2014 
      at the band's home studio in Las Vegas, Nevada`,
      description: `Self-produced by members of the band along with English hip-hop producer Alexander Grant, known by his moniker Alex da Kid, the album was released by Interscope Records and Grant's KIDinaKORNER label on February 17, 2015, in the United States.`,
      btnData: {
        label: 'PLAY',
        to: 'https://www.youtube.com/watch?v=rmtU2WJfPgU',
        background: buttonBG3,
      },
    },
    TimelineDateData: {
      timelineBottomSpace: 30,
      dateTextData: {
        date: 1290096094197,
      },
      title: 'Smoke + Mirrors',
    },
  },
];
