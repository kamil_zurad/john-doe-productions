import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';
export const StyledPageContentWrapper = styled.div`
  max-width: ${(props) => props.theme.wrapper.maxWidth}px;
  height: 100%;
  margin-left: auto;
  margin-right: auto;
  padding: 0 10px;

  ${MediaQueries.tablet} {
    padding: 0 50px;
  }

  ${MediaQueries.desktop} {
    padding: 0;
  }
`;
