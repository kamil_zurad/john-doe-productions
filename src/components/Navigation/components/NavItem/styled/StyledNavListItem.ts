import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';
export interface StyledNavListItemProps {
  isLast: boolean;
}

export const StyledNavListItem = styled.li<StyledNavListItemProps>`
  position: relative;
  margin-right: 0;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  align-items: center;
  height: auto;
  width: 100%;
  margin: 2px 0;

  ${MediaQueries.laptop} {
    flex-direction: row;
    width: auto;
  }
`;
