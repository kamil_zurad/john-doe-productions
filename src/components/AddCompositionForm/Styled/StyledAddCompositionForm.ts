import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';
import concertToursBG from 'assets/images/concertTours/concertToursBG.jpg';
export const StyledAddCompositionForm = styled.form`
  background-image: url(${concertToursBG});
  padding: 15px;
  background-position: center;
  width: 100%;
  height: 100%;
  ${MediaQueries.laptop} {
    width: auto;
    height: auto;
    min-height: 450px;
  }
`;
