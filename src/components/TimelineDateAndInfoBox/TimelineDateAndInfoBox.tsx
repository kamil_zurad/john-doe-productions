import TimelineDate, {
  TimelineDateProps,
} from 'components/TimelineDate/TimelineDate';
import InfoBox, { InfoBoxProps } from 'components/InfoBox/InfoBox';
import { StyledBox } from './TimelineDateAndInfoBox.styled';

export interface BoxProps {
  TimelineDateData: TimelineDateProps;
  vertical?: boolean;
  InfoBoxData: InfoBoxProps;
}

const Box: React.FC<BoxProps> = ({
  vertical,
  InfoBoxData,

  TimelineDateData,
}) => {
  return (
    <StyledBox vertical={vertical}>
      <TimelineDate {...TimelineDateData} />
      <InfoBox {...InfoBoxData} />
    </StyledBox>
  );
};

export default Box;
