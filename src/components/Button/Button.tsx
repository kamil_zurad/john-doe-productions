import { StyledButton } from './Button.styled';
export interface ButtonProps {
  onClickFunction: any;
  marginRight?: number;
  disabled?: boolean;
  background?: React.ReactChild;
}

const Button: React.FC<ButtonProps> = ({
  onClickFunction,
  marginRight,
  children,
  disabled,
  background,
}) => {
  return (
    <StyledButton
      onClick={onClickFunction}
      marginRight={marginRight}
      disabled={disabled}
      type="button"
      background={background}
    >
      {children}
    </StyledButton>
  );
};

export default Button;
