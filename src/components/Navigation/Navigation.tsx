import {
  StyledNav,
  StyledNavWrapper,
  StyledMobileIconsBar,
  StyledNavContentWrapper,
} from './Navigation.styled';
import BtnHamburger from '../BtnHamburger/BtnHamburger';
import NavItemsList from './components/NavItemsList/NavItemsList';
import { useCallback, useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { showMobileNavInstantly } from 'store/mobileNavScroll/mobileNavScroll.actions';
import { Breakpoints } from 'utils/MediaQueries';
import ViewEventConditionals from 'helpers/ViewEventConditionals';
import { navDesktopHeight } from 'utils/navHeight';
import {
  mobileNavIsNotVisible,
  mobileNavIsVisible,
} from 'store/mobileNavScroll/mobileNavScroll.actions';
export interface NavigationProps {}

const Navigation: React.FC<NavigationProps> = () => {
  const dispatch = useDispatch();

  const [isSticky, setIsSticky] = useState(false);

  const watchIfNavShouldBeSticky = useCallback(() => {
    if (window.pageYOffset > navDesktopHeight) {
      setIsSticky(true);
    } else {
      setIsSticky(false);
    }
  }, []);
  useEffect(() => {
    window.addEventListener('scroll', () => {
      watchIfNavShouldBeSticky();
    });
    return () => {
      window.removeEventListener('scroll', watchIfNavShouldBeSticky);
    };
  }, [watchIfNavShouldBeSticky]);

  let referenceScreenWidthRef = useRef(window.innerWidth);

  const resetSettingWhenCrossedThresholdPX = useCallback(() => {
    const referenceScreenWidth = referenceScreenWidthRef.current;
    const threshold = Breakpoints.laptop;
    const isResizedButStillAboveThreshold =
      ViewEventConditionals.isResizedButStillAboveThreshold(
        referenceScreenWidth,
        threshold,
      );
    const isResizedButStillBeneathThreshold =
      ViewEventConditionals.isResizedButStillBeneathThreshold(
        referenceScreenWidth,
        threshold,
      );
    const isResizedFromMobileToDesktop =
      ViewEventConditionals.isResizedFromMobileToDesktop(
        referenceScreenWidth,
        threshold,
      );

    if (isResizedButStillAboveThreshold) return;
    if (isResizedButStillBeneathThreshold) return;

    if (isResizedFromMobileToDesktop) {
      document.body.classList.remove('disableScroll');
      dispatch(mobileNavIsVisible());
    } else {
      dispatch(mobileNavIsNotVisible());
    }

    referenceScreenWidthRef.current = window.innerWidth;
    dispatch(showMobileNavInstantly());
  }, [dispatch]);

  const handleOnResize = useCallback(() => {
    resetSettingWhenCrossedThresholdPX();
  }, [resetSettingWhenCrossedThresholdPX]);
  useEffect(() => {
    window.addEventListener('resize', () => {
      handleOnResize();
    });
    return () => {
      window.removeEventListener('resize', handleOnResize);
    };
  }, [handleOnResize]);

  return (
    <StyledNav isSticky={isSticky}>
      <StyledNavWrapper>
        <StyledMobileIconsBar>
          <BtnHamburger />
        </StyledMobileIconsBar>
        <StyledNavContentWrapper>
          <NavItemsList />
        </StyledNavContentWrapper>
      </StyledNavWrapper>
    </StyledNav>
  );
};

export default Navigation;
