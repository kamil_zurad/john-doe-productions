import { StyledSubmitButton } from './SubmitButton.styled';
export interface ButtonProps {
  marginHorizontal?: number;
  disabled?: boolean;
  background?: React.ReactChild;
}

const SubmitButton: React.FC<ButtonProps> = ({
  marginHorizontal,
  children,
  disabled,
  background,
}) => {
  return (
    <StyledSubmitButton
      marginHorizontal={marginHorizontal}
      type="submit"
      disabled={disabled}
      background={background}
    >
      {children}
    </StyledSubmitButton>
  );
};

export default SubmitButton;
