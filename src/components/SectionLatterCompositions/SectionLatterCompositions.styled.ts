export { StyledLatterCompositionsWrapper } from './Styled/StyledLatterCompositionsWrapper';
export { StyledLatterCompositionsContent } from './Styled/StyledLatterCompositionsContent';
export { StyledAddCompositionBtnWrapper } from './Styled/StyledAddCompositionBtnWrapper';
