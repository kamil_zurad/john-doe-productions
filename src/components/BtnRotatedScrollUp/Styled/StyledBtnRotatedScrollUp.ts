import styled from 'styled-components';
import MediaQueries from 'utils/MediaQueries';
export interface StyledBtnRotatedScrollUpProps {
  width: number;
  isVisible: boolean;
}

export const StyledBtnRotatedScrollUp = styled.div<StyledBtnRotatedScrollUpProps>`
  display: none;

  ${MediaQueries.laptop} {
    display: block;
    position: fixed;
    bottom: 0;
    right: 0;
    width: ${({ width }) => width}px;
    height: ${({ width }) => width}px;
    transform: translate(-10%, -30%);
    color: rgba(255, 255, 255, 0.6);
    font-size: 24px;
    font-weight: bold;
    transition: 0.5s ease-out;
    -webkit-text-stroke: 1px rgba(0, 0, 0, 0.6);
    transition: opacity 0.7s linear;
    opacity: ${({ isVisible }) => (isVisible ? 1 : 0)};
    pointer-events: ${({ isVisible }) => (isVisible ? 'initial' : 'none')};
  }
`;
