import styled from 'styled-components';

export interface StyledCurvedLetterProps {
  $radius: number;
  $origin: number;
  isUpperCase?: boolean;
}

export const StyledCurvedLetter = styled.span<StyledCurvedLetterProps>`
  position: absolute;
  left: 50%;
  top: ${({ $radius }) => $radius / 2}px;
  height: ${({ $radius }) => $radius}px;
  text-transform: ${({ isUpperCase }) =>
    isUpperCase ? 'uppercase' : 'initial'};
  transform: translateY(-50%) rotate(${({ $origin }) => $origin}deg);
  transform-origin: 0 100%;
  user-select: none;
`;
