import { memo } from 'react';
import { StyledRotatedLettersBox } from './RotatedLettersBox.styled';

export interface RotatedLettersBoxProps {
  height: number;
  rotateDegValue: number;
  children: any;
}

const RotatedLettersBox: React.FC<RotatedLettersBoxProps> = ({
  height,
  rotateDegValue,
  children,
}) => {
  return (
    <StyledRotatedLettersBox
      height={height}
      style={{
        transform: `rotate(${rotateDegValue}deg)`,
      }}
    >
      {children}
    </StyledRotatedLettersBox>
  );
};

export default memo(RotatedLettersBox);
