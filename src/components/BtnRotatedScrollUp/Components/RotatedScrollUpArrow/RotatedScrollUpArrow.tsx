import { memo } from 'react';
import StyledRotatedScrollUpArrow from './RotatedScrollUpArrow.styled';

export interface RotatedScrollUpArrowProps {}

const RotatedScrollUpArrow: React.FC<RotatedScrollUpArrowProps> = () => {
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  };

  return (
    <StyledRotatedScrollUpArrow onClick={scrollToTop}>
      🡩
    </StyledRotatedScrollUpArrow>
  );
};

export default memo(RotatedScrollUpArrow);
