import SocialMediaCounter, {
  SocialMediaCounterProps,
} from 'components/SocialMediaCounter/SocialMediaCounter';

import { StyledSocialMediaCounters } from './SocialMediaCounters.styled';

export interface SocialMediaCountersProps {
  socialMediaItemsData: SocialMediaCounterProps[];
}

const SocialMediaCounters: React.FC<SocialMediaCountersProps> = ({
  socialMediaItemsData,
}) => {
  const socialMediaItems = socialMediaItemsData!.map(
    (item: SocialMediaCounterProps, itemIndex: number) => (
      <SocialMediaCounter
        likesNumber={item.likesNumber}
        icon={item.icon}
        key={itemIndex}
      />
    ),
  );

  return (
    <StyledSocialMediaCounters>{socialMediaItems}</StyledSocialMediaCounters>
  );
};

export default SocialMediaCounters;
